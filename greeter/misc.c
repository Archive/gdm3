/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * The dreaded miscellaneous category
 */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/utsname.h>

#include "gdmlogin.h"
#include "proto.h"
#include "gdm.h"

static const gchar RCSid[]="$Id$";


/**
 * gdm_login_done:
 *
 * Eat Flaming Death
 */

void
gdm_login_done (void)
{
    _exit (DISPLAY_SUCCESS);
}


/**
 * gdm_login_abort:
 *
 * Abort display logging an error message
 */

void
gdm_login_abort (const gchar *format, ...)
{
    va_list args;
    gchar *s;

    if (!format)
	exit (DISPLAY_ABORT);

    va_start (args, format);
    s = g_strdup_vprintf (format, args);
    va_end (args);
    
    syslog (LOG_ERR, "%s", s);
    closelog();

    exit (DISPLAY_ABORT);
}


/**
 * gdm_login_parse_enriched_string:
 * @s: String to parse
 *
 * Expand control chars in a string
 *
 * FIXME: Rewrite.  This code is evil.
 */

gchar *
gdm_login_parse_enriched_string (gchar *s)
{
    gchar cmd, *buffer, *start;
    gchar hostbuf[256];
    gchar *hostname, *temp1, *temp2, *display;
    struct utsname name;

    if (!s)
	return NULL;

    display = getenv ("DISPLAY");

    if (!display)
	return NULL;

    temp1 = strchr (display, '.');
    temp2 = strchr (display, ':');

    if (temp1)
	*temp1 = '\0';
    else if (temp2)
	*temp2 = '\0';
    else
	return NULL;

    gethostname (hostbuf, 255);
    hostname = g_strdup (hostbuf);
    
    if (!hostname) 
	hostname = g_strdup ("Gnome");

    uname (&name);

    if (strlen (s) > 1023) {
	syslog (LOG_ERR, _("gdm_parse_enriched_string: String too long!"));
	return g_strdup_printf (_("Welcome to %s"), hostname);
    }

    if (!(buffer = g_new0 (gchar, 4096))) {
	syslog (LOG_ERR, _("gdm_parse_enriched_string: Could not malloc temporary buffer!"));
	return NULL;
    }

    start = buffer;

    while (*s) {

	if (*s=='%' && (cmd = s[1]) != 0) {
	    s+=2;

	    switch (cmd) {

	    case 'h': 
		memcpy (buffer, hostname, strlen (hostname));
		buffer += strlen (hostname);
		break;
		
	    case 'n':
	        memcpy (buffer, name.nodename, strlen (name.nodename));
		buffer += strlen (name.nodename);
		break;

	    case 'd': 
		memcpy (buffer, display, strlen (display));
		buffer += strlen (display);
		break;

	    case 's':
	        memcpy (buffer, name.sysname, strlen (name.sysname));
		buffer += strlen (name.sysname);
		break;
		
	    case 'r':
	        memcpy (buffer, name.release, strlen (name.release));
	        buffer += strlen (name.release);
	        break;

	    case 'm':
	        memcpy (buffer, name.machine, strlen (name.machine));
	        buffer += strlen (name.machine);
	        break;

	    case '%':
		*buffer++ = '%';
		break;
		
	    default:
		break;
	    };
	}
	else
	    *buffer++ = *s++;
    }

    *buffer = 0;

    return g_strdup (start);
}


/* EOF */
