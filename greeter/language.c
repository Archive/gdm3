/* GDM - The Gnome Display Manager
 * Copyright (C) 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Present a language/locale selection requester */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gdmlogin.h"
#include "gdm.h"
#include "proto.h"

static const gchar RCSid[]="$Id$";


/* Externs */
extern gchar *curlang;
extern gchar *curuser;
extern GtkWidget *msg;
extern gboolean savelang;
extern gchar *language;
extern gchar *GdmDefaultLocale;
extern gboolean reqlock;

/* Globals */
static GtkWidget *save;
static GtkWidget *only;
static GtkWidget *cancel;
static GtkWidget *llist;
static GtkWidget *clist;
static gchar *lastlang;
static gchar *curlang  = NULL;
static gchar *language = NULL;


/**
 * gdm_lang_list_init:
 *
 * Populate the left clist with data from the language list
 */

static void
gdm_lang_list_init (void)
{
    GSList *l = gdm_locale_lang_list ();

    g_print ("glli: %lx\n", l);

    while (l && l->data) {
	GdmLang *lang = (GdmLang *) l->data;

	gtk_clist_append (GTK_CLIST (llist), &(lang->name));

	g_print ("%s\n", lang->name);

	l = l->next;
    }
}


/**
 * gdm_lang_list_handler:
 *
 * Lookup language and update the country selection list
 */

static void
gdm_lang_list_handler (GtkWidget *widget, gint row, gint column, GdkEventButton *event, gpointer data)
{
    gchar *text = NULL;
    GdmLang *lang = NULL;
    GSList *l = NULL;

    gtk_clist_get_text (GTK_CLIST (widget), row, column, &text);
    
    lang = gdm_locale_lang_lookup (text);

    if (!lang || !lang->locales)
	return;

    gtk_clist_clear (GTK_CLIST (clist));

    l = lang->locales;

    while (l && l->data) {
	GdmLocale *loc = (GdmLocale *) l->data;

	gtk_clist_append (GTK_CLIST (clist), &(loc->country));

	l = l->next;
    }
    
    return;
}


/**
 * gdm_lang_country_handler:
 *
 * Set preferred locale
 */

static void 
gdm_lang_country_handler (GtkWidget *widget) 
{
    gchar *s;

    if (!widget)
	return;

    gtk_label_get (GTK_LABEL (GTK_BIN (widget)->child), &curlang);
    s = g_strdup_printf (_("%s language selected"), curlang);
    gdm_gui_message_set (s);
    g_free (s);
}


/**
 * gdm_lang_request:
 *
 * Open locale selection requester
 */

void 
gdm_lang_req (void)
{
    GtkWidget *locreq;
    GtkWidget *frame1, *frame2;
    GtkWidget *vbox;
    GtkWidget *title;
    GtkWidget *help;
    GtkWidget *hbox;
    GtkWidget *lwin;
    GtkWidget *llabel;
    GtkWidget *cwin;
    GtkWidget *clabel;
    GtkWidget *buttonpane;
    GtkWidget *hline;
    GtkStyle  *style;

    /* Modal requester.  Return if requester lock is held */
    if (reqlock)
	return;

    reqlock = TRUE;

     /* Main window */
    locreq = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_container_border_width (GTK_CONTAINER (locreq), 0);

    /* 3D frame for main window */
    frame1 = gtk_frame_new (NULL);
    gtk_frame_set_shadow_type (GTK_FRAME (frame1), GTK_SHADOW_OUT);
    gtk_container_border_width (GTK_CONTAINER (frame1), 0);
    gtk_container_add (GTK_CONTAINER (locreq), frame1);
    gtk_widget_show (GTK_WIDGET (frame1));

    frame2 = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type (GTK_FRAME (frame2), GTK_SHADOW_IN);
    gtk_container_border_width (GTK_CONTAINER (frame2), 2);
    gtk_container_add (GTK_CONTAINER (frame1), frame2);
    gtk_widget_show (GTK_WIDGET (frame2));

    /* Vertical box containing browser box and button pane */
    vbox = gtk_vbox_new (FALSE, 10);
    gtk_container_border_width (GTK_CONTAINER (vbox), 10);
    gtk_container_add (GTK_CONTAINER (frame2), vbox);

    /* Title text */
    style = gtk_style_copy (locreq->style);

    gdk_font_unref (style->font);
    style->font = gdk_font_load ("-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*");

    if (style->font)
        gtk_widget_push_style (style);

    title = gtk_label_new (_("Language Selection"));
    gtk_misc_set_alignment (GTK_MISC (title), 0, 0.5);
    gtk_widget_show (title);
    gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (title), FALSE, FALSE, 0);

    gtk_widget_pop_style ();

    /* Separator */
    hline = gtk_hseparator_new ();
    gtk_widget_show (hline);
    gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (hline), FALSE, FALSE, 0);

    /* Help text */
    style = gtk_style_copy (locreq->style);

    gdk_font_unref (style->font);
    style->font = gdk_font_load ("-adobe-helvetica-medium-r-normal-*-10-*-*-*-*-*-*-*");

    if (style->font)
        gtk_widget_push_style (style);

    help = gtk_label_new (_("Select your preferred language to the left, " \
			    "then your country to the right."));
    gtk_label_set_line_wrap (GTK_LABEL (help), TRUE);
    gtk_misc_set_alignment (GTK_MISC (help), 0, 0.5);
    gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (help), FALSE, FALSE, 0);
    gtk_widget_show (help);

    gtk_widget_pop_style ();

    /* Language selection box */
    hbox = gtk_hbox_new (FALSE, 10);
    gtk_widget_ref (hbox);
    
    /* Language list */
    lwin = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (lwin),
				    GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_box_pack_start (GTK_BOX (hbox), lwin, TRUE, TRUE, 0);
    gtk_widget_show (lwin);

    llist = gtk_clist_new (1);
    gtk_widget_show (llist);
    gtk_container_add (GTK_CONTAINER (lwin), llist);
    gtk_container_set_border_width (GTK_CONTAINER (llist), 0);
    gtk_clist_set_column_width (GTK_CLIST (llist), 0, 80);
    gtk_clist_column_titles_show (GTK_CLIST (llist));
    
    llabel = gtk_label_new (_("Language"));
    gtk_widget_show (llabel);
    gtk_clist_set_column_widget (GTK_CLIST (llist), 0, llabel);
    gtk_clist_set_selection_mode (GTK_CLIST (llist), GTK_SELECTION_BROWSE);
    gdm_lang_list_init();

    gtk_signal_connect (GTK_OBJECT (llist),
			"select_row",
			GTK_SIGNAL_FUNC (gdm_lang_list_handler),
			NULL);

    /* Country list */
    cwin = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (cwin),
				    GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_box_pack_start (GTK_BOX (hbox), cwin, TRUE, TRUE, 0);
    gtk_widget_show (cwin);

    clist = gtk_clist_new (1);
    gtk_widget_show (clist);
    gtk_container_add (GTK_CONTAINER (cwin), clist);
    gtk_container_set_border_width (GTK_CONTAINER (clist), 0);
    gtk_clist_set_column_width (GTK_CLIST (clist), 0, 80);
    gtk_clist_column_titles_show (GTK_CLIST (clist));
    
    clabel = gtk_label_new (_("Country"));
    gtk_widget_show (clabel);
    gtk_clist_set_column_widget (GTK_CLIST (clist), 0, clabel);

    gtk_signal_connect (GTK_OBJECT (clist),
			"select_row",
			GTK_SIGNAL_FUNC (gdm_lang_country_handler),
			NULL);

    /* Pack listbox */
    gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (hbox), FALSE, FALSE, 0);
    gtk_widget_show (hbox);
    
    /* Buttons */
    save = gtk_button_new_with_label (_("Save"));
//    gtk_signal_connect (GTK_OBJECT (manage), "clicked",
//			GTK_SIGNAL_FUNC (gdm_locreq_manage), NULL);
    GTK_WIDGET_SET_FLAGS (GTK_WIDGET (save), GTK_CAN_DEFAULT);
    gtk_widget_show (GTK_WIDGET (save));

    only = gtk_button_new_with_label (_("Use"));
//    gtk_signal_connect(GTK_OBJECT (rescan), "clicked",
//		       GTK_SIGNAL_FUNC (gdm_locreq_xdmcp_discover), NULL);
    GTK_WIDGET_SET_FLAGS (GTK_WIDGET (only), GTK_CAN_DEFAULT);
    gtk_widget_show (GTK_WIDGET (only));

    cancel = gtk_button_new_with_label (_("Cancel"));
//    gtk_signal_connect(GTK_OBJECT (cancel), "clicked",
//		       GTK_SIGNAL_FUNC (gdm_locreq_cancel), NULL);
    GTK_WIDGET_SET_FLAGS(GTK_WIDGET (cancel), GTK_CAN_DEFAULT);
    gtk_widget_show (GTK_WIDGET (cancel));

    /* Button pane */
    buttonpane = gtk_hbox_new(TRUE, 0);
    gtk_container_set_border_width ( GTK_CONTAINER (buttonpane), 0);
    gtk_box_pack_start (GTK_BOX (buttonpane), 
			GTK_WIDGET (save), TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (buttonpane), 
			GTK_WIDGET (only), TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (buttonpane), 
			GTK_WIDGET (cancel), TRUE, TRUE, 0);
    gtk_window_set_default (GTK_WINDOW (locreq), GTK_WIDGET (save));
    gtk_widget_show_all (GTK_WIDGET (buttonpane));

    /* Put button pane in main window */
    gtk_box_pack_end (GTK_BOX (vbox), 
		      GTK_WIDGET (buttonpane), FALSE, FALSE, 0);


    gtk_widget_show (GTK_WIDGET (vbox));

    gtk_window_set_policy (GTK_WINDOW (locreq), 1, 1, 1);

    gtk_window_position (GTK_WINDOW (locreq), GTK_WIN_POS_CENTER);
    gtk_window_set_focus (GTK_WINDOW (locreq), llist);
    gtk_widget_show_all (GTK_WIDGET (locreq));

    reqlock = FALSE;
}


/**
 * gdm_lang_lookup:
 * @savedlang: Name of user's preferred language
 *
 * Check whether the user changed language selection
 */

void
gdm_lang_lookup (gchar* savedlang)
{
    if (!curuser)
	gdm_login_abort ("gdm_lang_lookup: curuser==NULL. Mail <mkp@mkp.net> with " \
			 "information on your PAM and user database setup");

    /* Don't save language unless told otherwise */
    savelang = FALSE;

    /* Previously saved language not found in ~user/.gnome/gdm */
    if (savedlang && ! strlen (savedlang)) {
	/* If "Last" is chosen use Default, else use current selection */
	if (!strcasecmp (curlang, lastlang))
	    language = GdmDefaultLocale;
	else
	    language = curlang;

	savelang = TRUE;
	return;
    }

    /* If a different language is selected */
    if (strcasecmp (curlang, lastlang)) {
	language = curlang;

	/* User's saved language is not the chosen one */
	if (strcasecmp (savedlang, language)) {
	    gchar *msg;

	    msg = g_strdup_printf (_("You have chosen %s for this session, but your default setting is " \
				     "%s.\nDo you wish to make %s the default for future sessions?"),
				   curlang, savedlang, curlang);
	    savelang = gdm_gui_query (msg);
	    g_free (msg);
	}
    }
    else
	language = savedlang;
}


/* EOF */
