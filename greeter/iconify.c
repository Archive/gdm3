/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * Iconify feature - Requested by Alan Cox for TV and fish...
 */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <gdk/gdkx.h>

#include "gdmlogin.h"
#include "proto.h"

static const gchar RCSid[]="$Id$";


/* External vars */
extern gint  GdmGrabFocus;
extern gchar *GdmIcon;

/* Local vars */
static GtkWidget *win   = NULL;
static GtkWidget *login = NULL;

typedef struct _cursoroffset { gint x,y; } CursorOffset;


/**
 * gdm_icon_pressed:
 *
 * Called when icon is clicked 
 */

static void
gdm_icon_pressed (GtkWidget *widget, GdkEventButton *event)
{
    CursorOffset *p;

    if (!widget || !event || !login)
	return;
    
    if (event->type == GDK_2BUTTON_PRESS) {
	gtk_widget_destroy (GTK_WIDGET (win));
	gdk_window_show (login->window);

	if (GdmGrabFocus)
	    XSetInputFocus(GDK_DISPLAY(), GDK_WINDOW_XWINDOW (login->window), 
			   RevertToNone, CurrentTime);
	return;
    }
    
    if (event->type != GDK_BUTTON_PRESS)
	return;
    
    p = gtk_object_get_user_data (GTK_OBJECT (widget));
    p->x = (gint) event->x;
    p->y = (gint) event->y;
    
    gtk_grab_add (widget);
    gdk_pointer_grab (widget->window, TRUE, GDK_BUTTON_RELEASE_MASK |
		      GDK_BUTTON_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK,
		      NULL, NULL, 0);
}


/**
 * gdm_icon_released:
 *
 * Called when icon is released
 */

static void
gdm_icon_released (GtkWidget *widget)
{
    if (!widget)
	return;

    gtk_grab_remove (widget);
    gdk_pointer_ungrab (0);
}


/**
 * gdm_icon_motion:
 *
 * Called on mouse movement
 */

static void
gdm_icon_motion (GtkWidget *widget, GdkEventMotion *event)
{
    gint xp, yp;
    CursorOffset *p;
    GdkModifierType mask;
    GdkWindow *rootwin;

    if (!widget || !event)
	return;

    rootwin = gdk_window_foreign_new (GDK_ROOT_WINDOW ());

    if (!rootwin)
	return;

    p = gtk_object_get_user_data (GTK_OBJECT (widget));
    gdk_window_get_pointer (rootwin, &xp, &yp, &mask);
    gtk_widget_set_uposition (GTK_WIDGET (widget), xp-p->x, yp-p->y);
}


/**
 * gdm_icon_handler:
 *
 * Event handler called when iconify is selected
 */

gboolean
gdm_icon_handler (GtkWidget *widget, gpointer data)
{
    GtkWidget *fixed;
    GtkWidget *icon;
    GdkGC *gc;
    GtkStyle *style;
    CursorOffset *icon_pos;
    gint rw, rh, iw, ih;

    if (!widget || !login)
	return TRUE;

    gdk_window_hide (login->window);
    style = gtk_widget_get_default_style();
    gc = style->black_gc; 
    win = gtk_window_new (GTK_WINDOW_POPUP);

    gtk_widget_set_events (win, 
			   gtk_widget_get_events (GTK_WIDGET (win)) | 
			   GDK_BUTTON_PRESS_MASK |
			   GDK_BUTTON_MOTION_MASK |
			   GDK_POINTER_MOTION_HINT_MASK);

    gtk_widget_realize (GTK_WIDGET (win));

    fixed = gtk_fixed_new();
    gtk_container_add (GTK_CONTAINER (win), fixed);
    gtk_widget_show (fixed);

    icon = gnome_pixmap_new_from_file (GdmIcon);
    gdk_window_get_size ((GdkWindow *) GNOME_PIXMAP (icon)->pixmap, &iw, &ih);
    
    gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET (icon), 0, 0);
    gtk_widget_show (GTK_WIDGET (icon));

    gtk_signal_connect (GTK_OBJECT (win), "button_press_event",
			GTK_SIGNAL_FUNC (gdm_icon_pressed),NULL);
    gtk_signal_connect (GTK_OBJECT (win), "button_release_event",
			GTK_SIGNAL_FUNC (gdm_icon_released),NULL);
    gtk_signal_connect (GTK_OBJECT (win), "motion_notify_event",
			GTK_SIGNAL_FUNC (gdm_icon_motion),NULL);

    icon_pos = g_new (CursorOffset, 1);
    gtk_object_set_user_data (GTK_OBJECT (win), icon_pos);

    gtk_widget_show (GTK_WIDGET (win));

    rw = gdk_screen_width();
    rh = gdk_screen_height();

    gtk_widget_set_uposition (GTK_WIDGET (win), rw-iw, rh-ih);

    return TRUE;
}


/**
 * gdm_icon_init:
 *
 * @widget: Pointer to the login window
 * 
 */

void
gdm_icon_init (GtkWidget *widget)
{
    if (!widget)
	return;

    login = widget;
}

/* EOF */
