/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * Gooey, eh
 */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/stat.h>

#include <gdk/gdkx.h>

#include "gdmlogin.h"
#include "gdm.h"
#include "proto.h"

static const gchar RCSid[]="$Id$";


/* Configuration options */
extern gchar *GdmGtkRC;
extern gchar *GdmIcon;
extern gchar *GdmLogo;
extern gint   GdmBrowser;
extern gint   GdmGrabFocus;
extern gint   GdmLangMenu;
extern gint   GdmSessionMenu;
extern gint   GdmSystemMenu;
extern gint   GdmIconMaxWidth;
extern gint   GdmIconMaxHeight;
extern gchar *GdmFont;
extern gchar *GdmWelcome;
extern gint   GdmXPosition;
extern gint   GdmYPosition;

/* External vars */
extern gchar *curuser;
extern GList *users;
extern gboolean reqlock;

/* Local vars */
static GtkWidget     *entry;	/* Entry field */
static GtkWidget     *label;	/* Entry field label */
static GtkWidget     *login;	/* The whole login window */
static GtkWidget     *msg;	/* Message field */
static GtkWidget     *sessmenu;	/* Session menu */
static GnomeIconList *browser;	/* Face browser */

/* Global */
gint           maxwidth;	/* Width of the widest icon */


/**
 * gdm_gui_shake_da_booty:
 *
 * Well, uh...
 */

void
gdm_gui_shake_da_booty (void)
{
    gint i, x, y;

    gdk_window_get_position (login->window, &x, &y);
    
    for (i=32 ; i > 0 ; i=i/2) {
	gdk_window_move (login->window, i+x, y);
	gdk_window_move (login->window, x, y);
	gdk_window_move (login->window, -i+x, y);
	gdk_window_move (login->window, x, y);
    }
}


/**
 * gdm_gui_label_set:
 * @text: Text to put in entry label
 *
 * Update entry label
 */

void
gdm_gui_label_set (gchar *text)
{
    if (!text)
	return;

    gtk_label_set (GTK_LABEL (label), text);
    gtk_widget_show (GTK_WIDGET (label));
}


/**
 * gdm_gui_entry_set:
 * @text: Text to put in entry field
 * @visible: Whether the text should be visible or not
 *
 * Update entry field
 */

void
gdm_gui_entry_set (gchar *text, gboolean visible)
{
    if (!text)
	return;

    gtk_entry_set_text (GTK_ENTRY (entry), text);
    gtk_entry_set_visibility (GTK_ENTRY (entry), visible);
    gtk_entry_set_max_length (GTK_ENTRY (entry), FIELD_SIZE-1);
    gtk_widget_set_sensitive (GTK_WIDGET (entry), TRUE);
    gtk_window_set_focus (GTK_WINDOW (login), entry);	
    gtk_widget_show (GTK_WIDGET (entry));
}


/**
 * gdm_gui_message_set:
 * @text: Text to put in message label
 *
 * Update message label
 */

void
gdm_gui_message_set (gchar *text)
{
    if (!text)
	return;

    gtk_label_set (GTK_LABEL (msg), text);
    gtk_widget_show (GTK_WIDGET (msg));
}


/**
 * gdm_gui_sessmenu_deactivate:
 *
 * Disable session menu temporarily
 */

void
gdm_gui_sessmenu_deactivate (void)
{
    gtk_widget_set_sensitive (GTK_WIDGET (sessmenu), FALSE);
}


/**
 * gdm_gui_browser_activate:
 *
 * Enable face browsing
 */

void
gdm_gui_browser_activate (void)
{
    gtk_widget_set_sensitive (GTK_WIDGET (browser), TRUE);
}


/**
 * gdm_gui_browser_update:
 *
 * Refill the face browser
 */

static void
gdm_gui_browser_update (void)
{
    GList *list = users;

    gnome_icon_list_clear (GNOME_ICON_LIST (browser));

    while (list) {
	GdmLoginUser *user = list->data;

	gnome_icon_list_append_imlib (GNOME_ICON_LIST (browser), user->picture, user->login);
	list = list->next;
    }

    gnome_icon_list_thaw (GNOME_ICON_LIST (browser));
}


/**
 * gdm_gui_browser_select:
 *
 * Face browser select event handler
 */

static gboolean 
gdm_gui_browser_select (GtkWidget *widget, gint selected, GdkEvent *event)
{
    GdmLoginUser *user;

    if (!widget || !event)
	return TRUE;

    switch (event->type) {
	    
    case GDK_BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
	user = g_list_nth_data (users, selected);

	if (user && user->login)
	    gtk_entry_set_text (GTK_ENTRY (entry), user->login);

	break;

    case GDK_2BUTTON_PRESS:
	user = g_list_nth_data (users, selected);

	if (user && user->login)
	    gtk_entry_set_text (GTK_ENTRY (entry), user->login);

	if (!curuser)
	    curuser = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

	gtk_widget_set_sensitive (GTK_WIDGET (entry), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (browser), FALSE);
	g_print ("%c%s\n", STX, gtk_entry_get_text (GTK_ENTRY (entry)));
	break;
	
    default: 
	break;
    }
    
    return TRUE;
}


/**
 * gdm_gui_browser_unselect:
 *
 * Face browser unselect event handler
 */

static gboolean
gdm_gui_browser_unselect (GtkWidget *widget, gint selected, GdkEvent *event)
{
    if (!widget || !event)
	return TRUE;

    switch (event->type) {
	    
    case GDK_BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
	gtk_entry_set_text (GTK_ENTRY (entry), "");
	break;
	
    default:
	break;
    }

    return TRUE;
}


/**
 * gdm_gui_entry_handler:
 *
 * Entry field input event handler
 */

static gboolean
gdm_gui_entry_handler (GtkWidget *widget, GdkEventKey *event)
{
    if (!event)
	return TRUE;

    switch (event->keyval) {

    case GDK_Return:
	gtk_widget_set_sensitive (GTK_WIDGET (entry), FALSE);

	if (GdmBrowser)
	    gtk_widget_set_sensitive (GTK_WIDGET (browser), FALSE);

	/* Save login. I'm making the assumption that login is always
	 * the first thing entered. This might not be true for all PAM
	 * setups. Needs thinking! 
	 */

	if (!curuser)
	    curuser = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

	g_print ("%c%s\n", STX, gtk_entry_get_text (GTK_ENTRY (entry)));
	break;

    case GDK_Up:
    case GDK_Down:
    case GDK_Tab:
	gtk_signal_emit_stop_by_name (GTK_OBJECT (entry), "key_press_event");
	break;

    case GDK_r:			/* Hotkey for aborting display */
	if (event->state & GDK_CONTROL_MASK)
	exit (EXIT_SUCCESS);

    default:
	break;
    }

    return TRUE;
}


/**
 * gdm_gui_query:
 * @text: Question to be asked
 *
 * Displays a modal requester and returns result.
 */

gboolean
gdm_gui_query (gchar *text)
{
    GtkWidget *req;

    req = gnome_message_box_new (text,
				 GNOME_MESSAGE_BOX_QUESTION,
				 GNOME_STOCK_BUTTON_YES,
				 GNOME_STOCK_BUTTON_NO,
				 NULL);
	    
    gtk_window_set_modal (GTK_WINDOW (req), TRUE);
    gtk_window_set_position (GTK_WINDOW (req), GTK_WIN_POS_CENTER);

    return !gnome_dialog_run (GNOME_DIALOG(req));
}


/**
 * gdm_gui_reboot_handler:
 *
 * Ask whether user wants to reboot or not
 */

static gboolean
gdm_gui_reboot_handler (void)
{
    if (reqlock)
	return TRUE;

    reqlock = TRUE;

    if (gdm_gui_query (_("Are you sure you want to reboot the machine?"))) {
	closelog();
	exit (DISPLAY_REBOOT);
    }

    reqlock = FALSE;

    return TRUE;
}


/**
 * gdm_gui_halt_handler:
 *
 * Ask whether user wants to halt the system or not
 */

static gboolean
gdm_gui_halt_handler (void)
{
    if (reqlock)
	return TRUE;

    reqlock = TRUE;

    if (gdm_gui_query (_("Are you sure you want to halt the machine?"))) {
	closelog();
	exit (DISPLAY_HALT);
    }

    reqlock = FALSE;

    return TRUE;
}


/**
 * gdm_gui_init:
 *
 * A maze of twisty little passages, all alike...
 *
 * This must be glade'ified
 */

void
gdm_gui_init (void)
{
    GtkWidget *frame1, *frame2;
    GtkWidget *mbox, *menu, *menubar, *item, *welcome;
    GtkWidget *table, *stack, *hline1, *hline2;
    GtkWidget *bbox = NULL;
    GtkWidget *logoframe = NULL;
    GtkStyle *style;
    GtkAccelGroup *accel;
    gchar *greeting;
    gint cols, rows;
    struct stat statbuf;

    if (*GdmGtkRC)
	gtk_rc_parse (GdmGtkRC);

    login = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_widget_ref (login);
    gtk_object_set_data_full (GTK_OBJECT (login), "login", login,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_window_set_title (GTK_WINDOW (login), "GDM Login");

    accel = gtk_accel_group_new();
    gtk_window_add_accel_group(GTK_WINDOW (login), accel);

    frame1 = gtk_frame_new (NULL);
    gtk_frame_set_shadow_type (GTK_FRAME (frame1), GTK_SHADOW_OUT);
    gtk_container_border_width (GTK_CONTAINER (frame1), 0);
    gtk_container_add (GTK_CONTAINER (login), frame1);
    gtk_object_set_data_full (GTK_OBJECT (login), "frame1", frame1,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_ref (GTK_WIDGET (frame1));
    gtk_widget_show (GTK_WIDGET (frame1));

    frame2 = gtk_frame_new (NULL);
    gtk_frame_set_shadow_type (GTK_FRAME (frame2), GTK_SHADOW_IN);
    gtk_container_border_width (GTK_CONTAINER (frame2), 2);
    gtk_container_add (GTK_CONTAINER (frame1), frame2);
    gtk_object_set_data_full (GTK_OBJECT (login), "frame2", frame2,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_ref (GTK_WIDGET (frame2));
    gtk_widget_show (GTK_WIDGET (frame2));

    mbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (mbox);
    gtk_object_set_data_full (GTK_OBJECT (login), "mbox", mbox,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (mbox);
    gtk_container_add (GTK_CONTAINER (frame2), mbox);

    menubar = gtk_menu_bar_new();
    gtk_widget_ref (GTK_WIDGET (menubar));
    gtk_box_pack_start (GTK_BOX (mbox), menubar, FALSE, FALSE, 0);

    if (GdmSessionMenu) {
	menu = gtk_menu_new();
	gdm_session_init (menu);
	sessmenu = gtk_menu_item_new_with_label (_("Session"));
	gtk_menu_bar_append (GTK_MENU_BAR(menubar), sessmenu);
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (sessmenu), menu);
	gtk_widget_add_accelerator( sessmenu, "activate_item", accel,
				    GDK_Escape, 0, 0);
	gtk_widget_add_accelerator( sessmenu, "activate_item", accel,
				    GDK_s, GDK_MOD1_MASK, 0);
	gtk_widget_show (GTK_WIDGET (sessmenu));
    }

    if (GdmLangMenu) {
	    item = gtk_menu_item_new_with_label (_("Locale..."));
	    gtk_menu_bar_append (GTK_MENU_BAR (menubar), item);
	    gtk_signal_connect (GTK_OBJECT (item), "activate",
				GTK_SIGNAL_FUNC (gdm_lang_req), 
				NULL);
	    gtk_widget_show (GTK_WIDGET (item));
    }

    if (GdmSystemMenu) {
	menu = gtk_menu_new();
	item = gtk_menu_item_new_with_label (_("Reboot..."));
	gtk_menu_append (GTK_MENU (menu), item);
	gtk_signal_connect (GTK_OBJECT (item), "activate",
			    GTK_SIGNAL_FUNC (gdm_gui_reboot_handler), 
			    NULL);
	gtk_widget_show (GTK_WIDGET (item));
	
	item = gtk_menu_item_new_with_label (_("Halt..."));
	gtk_menu_append (GTK_MENU (menu), item);
	gtk_signal_connect (GTK_OBJECT (item), "activate",
			   GTK_SIGNAL_FUNC (gdm_gui_halt_handler), 
			    NULL);
	gtk_widget_show (GTK_WIDGET (item));
	
	item = gtk_menu_item_new_with_label (_("System"));
	gtk_menu_bar_append (GTK_MENU_BAR (menubar), item);
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), menu);
	gtk_widget_add_accelerator( item, "activate_item", accel,
	    GDK_y, GDK_MOD1_MASK, 0);
	gtk_widget_show (GTK_WIDGET (item));
    }

    if (*GdmIcon) {
	if (access (GdmIcon, R_OK)) {
	    syslog (LOG_WARNING, _("Can't open icon file: %s. Suspending iconify feature!"), 
		    GdmIcon);
	}
	else {
	    gdm_icon_init (login);
	    item = gtk_menu_item_new_with_label (_("Iconify"));
	    gtk_menu_bar_append (GTK_MENU_BAR (menubar), item);
	    gtk_signal_connect (GTK_OBJECT (item), "activate",
				GTK_SIGNAL_FUNC (gdm_icon_handler), 
				NULL);
	    gtk_widget_show (GTK_WIDGET (item));
	}
    }

    if (GdmBrowser)
	rows = 2;
    else
	rows = 1;

    if (*GdmLogo && ! stat (GdmLogo, &statbuf))
	cols = 2;
    else 
	cols = 1;

    table = gtk_table_new (rows, cols, FALSE);
    gtk_widget_ref (table);
    gtk_object_set_data_full (GTK_OBJECT (login), "table", table,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (table);
    gtk_box_pack_start (GTK_BOX (mbox), table, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER (table), 10);
    gtk_table_set_row_spacings (GTK_TABLE (table), 10);
    gtk_table_set_col_spacings (GTK_TABLE (table), 10);

    if (GdmBrowser) {
	GtkStyle *style;
	GdkColor  bbg = { 0, 0xFFFF, 0xFFFF, 0xFFFF };
	GtkWidget *bframe;
	GtkWidget *scrollbar;

	gdm_user_init();

	/* Find background style for browser */
	style = gtk_style_copy (login->style);
	style->bg[GTK_STATE_NORMAL] = bbg;
	gtk_widget_push_style (style);
	
	/* Icon list */
	if (maxwidth < GdmIconMaxWidth/2)
	    maxwidth = (gint) GdmIconMaxWidth/2;
	
	browser = GNOME_ICON_LIST (gnome_icon_list_new (maxwidth+20, NULL, FALSE));
	gnome_icon_list_freeze (GNOME_ICON_LIST (browser));
	gnome_icon_list_set_separators (GNOME_ICON_LIST (browser), " /-_.");
	gnome_icon_list_set_row_spacing (GNOME_ICON_LIST (browser), 2);
	gnome_icon_list_set_col_spacing (GNOME_ICON_LIST (browser), 2);
	gnome_icon_list_set_icon_border (GNOME_ICON_LIST (browser), 2);
	gnome_icon_list_set_text_spacing (GNOME_ICON_LIST (browser), 2);
	gnome_icon_list_set_selection_mode (GNOME_ICON_LIST (browser), GTK_SELECTION_SINGLE);
	gtk_signal_connect (GTK_OBJECT (browser), "select_icon",
			    GTK_SIGNAL_FUNC (gdm_gui_browser_select), NULL);
	gtk_signal_connect (GTK_OBJECT (browser), "unselect_icon",
			    GTK_SIGNAL_FUNC (gdm_gui_browser_unselect), NULL);
	gtk_widget_pop_style();
	
	/* Browser 3D frame */
	bframe = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (bframe), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER(bframe), GTK_WIDGET (browser));
	
	/* Browser scroll bar */
	scrollbar = gtk_vscrollbar_new (browser->adj);
	
	/* Box containing all browser functionality */
	bbox = gtk_hbox_new (0, 0);
	gtk_box_pack_start (GTK_BOX (bbox), GTK_WIDGET (bframe), 1, 1, 0);
	gtk_box_pack_start (GTK_BOX (bbox), GTK_WIDGET (scrollbar), 0, 0, 0);
	gtk_widget_show_all (GTK_WIDGET (bbox));

	/* FIXME */

	gtk_widget_set_usize (GTK_WIDGET (bbox),
			      (gint) gdk_screen_width () * 0.5,
			      (gint) gdk_screen_height () * 0.25);

	gdm_gui_browser_update();
    }

    if (*GdmLogo && !access (GdmLogo, R_OK)) {
	GtkWidget *logo;

	logoframe = gtk_frame_new (NULL);
	gtk_widget_ref (logoframe);
	gtk_object_set_data_full (GTK_OBJECT (login), "logoframe", logoframe,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (logoframe);
	gtk_frame_set_shadow_type (GTK_FRAME (logoframe), GTK_SHADOW_IN);

	logo = gnome_pixmap_new_from_file (GdmLogo);
	gtk_container_add (GTK_CONTAINER (logoframe), logo);
	gtk_widget_show (GTK_WIDGET (logo));
    }

    stack = gtk_table_new (6, 1, FALSE);
    gtk_widget_ref (stack);
    gtk_object_set_data_full (GTK_OBJECT (login), "stack", stack,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (stack);

    style = gtk_style_new();
    gdk_font_unref (style->font);
    style->font = gdk_font_load (GdmFont);

    if (style->font)
	gtk_widget_push_style (style);

    greeting = gdm_login_parse_enriched_string (GdmWelcome);    
    welcome = gtk_label_new (greeting);
    gtk_widget_set_name(welcome, "Welcome");
    g_free(greeting);
    gtk_widget_ref (welcome);
    gtk_object_set_data_full (GTK_OBJECT (login), "welcome", welcome,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (welcome);
    gtk_table_attach (GTK_TABLE (stack), welcome, 0, 1, 0, 1,
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

    gtk_widget_pop_style ();

    hline1 = gtk_hseparator_new ();
    gtk_widget_ref (hline1);
    gtk_object_set_data_full (GTK_OBJECT (login), "hline1", hline1,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (hline1);
    gtk_table_attach (GTK_TABLE (stack), hline1, 0, 1, 1, 2,
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		      (GtkAttachOptions) (GTK_FILL), 0, 10);
    
    label = gtk_label_new (_("Login:"));
    gtk_widget_ref (label);
    gtk_object_set_data_full (GTK_OBJECT (login), "label", label,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (label);
    gtk_table_attach (GTK_TABLE (stack), label, 0, 1, 2, 3,
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		      (GtkAttachOptions) (0), 0, 0);
    gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
    gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
    gtk_misc_set_padding (GTK_MISC (label), 10, 5);
    
    entry = gtk_entry_new_with_max_length (FIELD_SIZE-1);
    gtk_widget_ref (entry);
    gtk_object_set_data_full (GTK_OBJECT (login), "entry", entry,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_entry_set_text (GTK_ENTRY (entry), "");
    gtk_widget_show (entry);
    gtk_table_attach (GTK_TABLE (stack), entry, 0, 1, 3, 4,
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		      (GtkAttachOptions) (0), 10, 0);
    gtk_signal_connect_object (GTK_OBJECT(entry), 
			       "key_press_event", 
			       GTK_SIGNAL_FUNC (gdm_gui_entry_handler),
			       NULL);
    
    hline2 = gtk_hseparator_new ();
    gtk_widget_ref (hline2);
    gtk_object_set_data_full (GTK_OBJECT (login), "hline2", hline2,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (hline2);
    gtk_table_attach (GTK_TABLE (stack), hline2, 0, 1, 4, 5,
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		      (GtkAttachOptions) (GTK_FILL), 0, 10);
        
    msg = gtk_label_new (_("Please log in"));
    gtk_widget_set_name(msg, "Message");
    gtk_widget_ref (msg);
    gtk_object_set_data_full (GTK_OBJECT (login), "msg", msg,
			      (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (msg);
    gtk_table_attach (GTK_TABLE (stack), msg, 0, 1, 5, 6,
		      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		      (GtkAttachOptions) (GTK_FILL), 10, 10);

    /* Put it nicely together */
    if (GdmBrowser && *GdmLogo) {
	gtk_table_attach (GTK_TABLE (table), bbox, 0, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_table_attach (GTK_TABLE (table), logoframe, 0, 1, 1, 2,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_table_attach (GTK_TABLE (table), stack, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
    }
    else if (GdmBrowser) {
	gtk_table_attach (GTK_TABLE (table), bbox, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_table_attach (GTK_TABLE (table), stack, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
    }
    else if (*GdmLogo) {
	gtk_table_attach (GTK_TABLE (table), logoframe, 0, 1, 0, 1,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_table_attach (GTK_TABLE (table), stack, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
    }
    else
	gtk_table_attach (GTK_TABLE (table), stack, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
    
    gtk_window_set_focus (GTK_WINDOW (login), entry);	
    gtk_window_set_policy (GTK_WINDOW (login), 1, 1, 1);

    if (GdmXPosition && GdmYPosition)
	gtk_widget_set_uposition (GTK_WIDGET (login), GdmXPosition, GdmYPosition);
    else
	gtk_window_position (GTK_WINDOW (login), GTK_WIN_POS_CENTER);

    gtk_widget_show_all (GTK_WIDGET (login));

    if (GdmGrabFocus)
	XSetInputFocus(GDK_DISPLAY(), GDK_WINDOW_XWINDOW (login->window), 
		       RevertToNone, CurrentTime);
}

/* EOF */
