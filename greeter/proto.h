/* GDM - The Gnome Display Manager
 * Copyright (C) 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GDM_PROTO_H__
#define __GDM_PROTO_H__

#include <gnome.h>

void     gdm_gui_shake_da_booty(void);
void     gdm_gui_label_set (gchar *text);
void     gdm_gui_entry_set (gchar *text, gboolean visible);
void     gdm_gui_message_set (gchar *text);
void     gdm_gui_sessmenu_deactivate (void);
void     gdm_gui_browser_activate (void);
gboolean gdm_gui_query (gchar *text);
void     gdm_gui_init (void);

gboolean gdm_icon_handler (GtkWidget *widget, gpointer data);
void     gdm_icon_init (GtkWidget *widget);

void     gdm_lang_lookup (gchar* savedlang);
void     gdm_lang_req (void);

void     gdm_locale_init (void);
GdmLang *gdm_locale_lang_lookup (const gchar *name);
GSList  *gdm_locale_lang_list (void);

void     gdm_session_init (GtkWidget *menu);
void     gdm_session_handler (GtkWidget *widget);
gboolean gdm_session_save (void);
gchar   *gdm_session_lookup (gchar* savedsess);

void     gdm_user_init (void);

void     gdm_login_abort (const gchar *format, ...);
void     gdm_login_done (void);

gchar   *gdm_login_parse_enriched_string (gchar *s);

#endif /* __GDM_PROTO_H__ */

/* EOF */
