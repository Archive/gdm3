/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <syslog.h>
#include <ctype.h>
#include <signal.h>

#include "gdmlogin.h"
#include "gdm.h"
#include "proto.h"
#include "filecheck.h"

static const gchar RCSid[]="$Id$";

/* Configuration options */
gint  GdmBrowser;
gint  GdmGrabFocus;
gint  GdmDebug;
gint  GdmIconMaxHeight;
gint  GdmIconMaxWidth;
gint  GdmQuiver;
gint  GdmSystemMenu;
gint  GdmSessionMenu;
gint  GdmLangMenu;
gint  GdmXPosition;
gint  GdmYPosition;
gchar *GdmLogo;
gchar *GdmWelcome;
gchar *GdmFont;
gchar *GdmGtkRC;
gchar *GdmIcon;
gchar *GdmSessionDir;
gchar *GdmLocaleFile;
gchar *GdmDefaultLocale;
gchar *GdmExclude;
gchar *GdmInclude;
gchar *GdmGlobalFaceDir;
gchar *GdmDefaultFace;

/* Workaround for filecheck.o */
gint GdmRelaxPerms;
gint GdmUserMaxFile;

/* Global vars */
gchar *curuser    = NULL;	/* The username currently trying to log in */
GList *users      = NULL;	/* List of users on the system in browser mode */
gboolean savelang = FALSE;	/* User wants his language selection saved */
gboolean reqlock  = FALSE;	/* Held when a requester is open */


/**
 * gdm_login_parse_config:
 *
 * Parse the GDM configuration file
 */

static void 
gdm_login_parse_config (void)
{
    gchar *display;
    struct stat unused;
	
    if (stat (GDM_CONFIG_FILE, &unused) == -1)
	gdm_login_abort (_("gdm_login_parse_config: No configuration file: %s. Aborting."), 
			 GDM_CONFIG_FILE);

    gnome_config_push_prefix ("=" GDM_CONFIG_FILE "=/");
    GdmBrowser = gnome_config_get_int (GDM_KEY_BROWSER);
    GdmGrabFocus = gnome_config_get_int (GDM_KEY_GRAB);
    GdmLogo = gnome_config_get_string (GDM_KEY_LOGO);
    GdmFont = gnome_config_get_string (GDM_KEY_FONT);
    GdmIcon = gnome_config_get_string (GDM_KEY_ICON);
    GdmQuiver = gnome_config_get_int (GDM_KEY_QUIVER);
    GdmSystemMenu = gnome_config_get_int (GDM_KEY_SYSMENU);
    GdmSessionMenu = gnome_config_get_int (GDM_KEY_SESSMENU);
    GdmLangMenu = gnome_config_get_int (GDM_KEY_LANGMENU);
    GdmUserMaxFile = gnome_config_get_int (GDM_KEY_MAXFILE);
    GdmRelaxPerms = gnome_config_get_int (GDM_KEY_RELAXPERM);
    GdmLocaleFile = gnome_config_get_string (GDM_KEY_LOCFILE);
    GdmDefaultLocale = gnome_config_get_string (GDM_KEY_LOCALE);
    GdmSessionDir = gnome_config_get_string (GDM_KEY_SESSDIR);
    GdmWelcome = gnome_config_get_string (GDM_KEY_WELCOME);
    GdmGtkRC = gnome_config_get_string (GDM_KEY_GTKRC);
    GdmExclude = gnome_config_get_string (GDM_KEY_EXCLUDE);
    GdmInclude = gnome_config_get_string (GDM_KEY_INCLUDE);
    GdmGlobalFaceDir = gnome_config_get_string (GDM_KEY_FACEDIR);
    GdmDefaultFace = gnome_config_get_string (GDM_KEY_FACE);
    GdmDebug = gnome_config_get_int (GDM_KEY_DEBUG);
    GdmUserMaxFile = gnome_config_get_int (GDM_KEY_MAXFILE);
    GdmRelaxPerms = gnome_config_get_int (GDM_KEY_RELAXPERM);
    GdmIconMaxWidth = gnome_config_get_int (GDM_KEY_ICONWIDTH);
    GdmIconMaxHeight = gnome_config_get_int (GDM_KEY_ICONHEIGHT);
    GdmXPosition = gnome_config_get_int (GDM_KEY_XPOSITION);
    GdmYPosition = gnome_config_get_int (GDM_KEY_YPOSITION);
    gnome_config_pop_prefix();

    if (!GdmLocaleFile || stat (GdmLocaleFile, &unused) == -1)
	gdm_login_abort ("gdm_login_parse_config: Could not open locale file %s. Aborting!", GdmLocaleFile);

    /* Disable System menu on non-local displays */
    display = getenv ("DISPLAY");

    if (!display)
	gdm_login_abort ("gdm_login_parse_config: DISPLAY variable not set!");

    if (strncmp (display, ":", 1))
	GdmSystemMenu = 0;
}


/**
 * gdm_login_ctrl_handler:
 *
 * I/O dispatch and control
 */

static gboolean
gdm_login_ctrl_handler (GIOChannel *source, GIOCondition cond, gint fd)
{
    gchar buf[PIPE_SIZE];
    gint len;

    /* If this is not incoming i/o then return */
    if (cond != G_IO_IN) 
	return TRUE;

    /* Read random garbage from i/o channel until STX is found */
    do {
	g_io_channel_read (source, buf, 1, &len);

	if (len != 1)
	    return TRUE;

    } 
    while (buf[0] && buf[0] != STX);

    /* Read opcode */
    g_io_channel_read (source, buf, 1, &len);

    /* If opcode couldn't be read */
    if (len != 1)
	return TRUE;

    /* Parse opcode */
    switch (buf[0]) {

    case GDM_PROMPT:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len);
	buf[len-1] = '\0';

	/* Update label according to PAM and empty entry field */
	gdm_gui_label_set (buf);
	gdm_gui_entry_set ("", TRUE);

	/* Assume that if there is no current user name we are asking
	 * for the user name, and want the browser enabled. This can't
	 * be done in GDM_RESET, because the parent may be sleeping
	 * for GdmRetry seconds
	 *
	 * Note that this relies on the PAM setup asking for username
	 * first, so if the design is rethought for that this will
	 * need to change too!  
	 */
	if (!curuser && GdmBrowser)
	    gdm_gui_browser_activate();

	break;

    case GDM_NOECHO:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len);
	buf[len-1] = '\0';

	/* Update label according to PAM, empty entry field and turn
         * echo off 
	 */
	gdm_gui_label_set (buf);
	gdm_gui_entry_set ("", FALSE);

	break;

    case GDM_MSGERR:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len);
	buf[len-1] = '\0';

	/* Update message with whatever PAM wants to put there */
	gdm_gui_message_set (buf);
	break;

    case GDM_SESS:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len); /* Empty */
	buf[len-1] = '\0';

	/* Pass user's session of choice to the daemon */
	g_print ("%c%s\n", STX, gdm_session_lookup (buf));
	break;

    case GDM_LANG:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len); /* Empty */
	buf[len-1] = '\0';
	gdm_lang_lookup (buf);

	/* Pass user's locale to the daemon */
//	g_print ("%c%s\n", STX, gdm_language_lookup ());
	break;

    case GDM_SSESS:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len); /* Empty */

	/* Inform daemon whether user's session should be saved or not */
	if (gdm_session_save())
	    g_print ("%cY\n", STX);
	else
	    g_print ("%c\n", STX);
	
	break;

    case GDM_SLANG:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len); /* Empty */

	/* Inform daemon whether user's locale should be saved or not */
	if (savelang)
	    g_print ("%cY\n", STX);
	else
	    g_print ("%c\n", STX);

	break;

    case GDM_RESET:
	g_io_channel_read (source, buf, PIPE_SIZE-1, &len);
	buf[len-1] = '\0';

	/* Authentication failed, restart login process */
	if (GdmQuiver)
	    gdm_gui_shake_da_booty();

	if (curuser) {
	    g_free (curuser);
	    curuser = NULL;
	}

	g_print ("%c\n", STX);
	break;

    case GDM_QUIT:
	/* Wish me luck as you wave me goodbye */
	exit (EXIT_SUCCESS);
	break;
	
    default:
	break;
    }

    return TRUE;
}


/**
 * main:
 *
 * Nothing to see here, move along
 */

int 
main (int argc, char *argv[])
{
    gchar **fixedargv;
    gint fixedargc, i;
    struct sigaction hup;
    sigset_t mask;
    GIOChannel *ctrlch;

    /* Avoid creating ~gdm/.gnome stuff */
    gnome_do_not_create_directories = TRUE;

    /* Open syslog */
    openlog ("gdmlogin", LOG_PID, LOG_DAEMON);

    /* Add options before firing off gnome_init */
    fixedargc = argc + 1;
    fixedargv = g_new0 (gchar *, fixedargc);

    for (i=0; i < argc; i++)
	fixedargv[i] = argv[i];
    
    fixedargv[fixedargc-1] = "--disable-sound";
    gnome_init ("gdmlogin", VERSION, fixedargc, fixedargv);
    g_free (fixedargv);

    gnome_preferences_set_dialog_position (GTK_WIN_POS_CENTER);

    /* Localization */
    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);

    /* Init subsystems */
    gdm_login_parse_config();
    gdm_locale_init();
    gdm_gui_init();

    /* Signal handling */
    hup.sa_handler = (void *) gdm_login_done;
    hup.sa_flags = 0;
    sigemptyset (&hup.sa_mask);

    if (sigaction (SIGHUP, &hup, NULL) < 0) 
        gdm_login_abort (_("main: Error setting up HUP signal handler"));

    if (sigaction (SIGINT, &hup, NULL) < 0) 
        gdm_login_abort (_("main: Error setting up INT signal handler"));

    if (sigaction (SIGTERM, &hup, NULL) < 0) 
        gdm_login_abort (_("main: Error setting up TERM signal handler"));

    sigfillset (&mask);
    sigdelset  (&mask, SIGTERM);
    sigdelset  (&mask, SIGHUP);
    sigdelset  (&mask, SIGINT);
    
    if (sigprocmask (SIG_SETMASK, &mask, NULL) == -1) 
	gdm_login_abort (_("Could not set signal mask!"));

    /* Pipe control handler */
    ctrlch = g_io_channel_unix_new (STDIN_FILENO);
    g_io_channel_init (ctrlch);
    g_io_add_watch (ctrlch, 
		    G_IO_IN|G_IO_PRI|G_IO_ERR|G_IO_HUP|G_IO_NVAL,
		    (GIOFunc) gdm_login_ctrl_handler,
		    NULL);
    g_io_channel_unref (ctrlch);

    /* Ignition */
    gtk_main();

    exit (EXIT_SUCCESS);
}

/* EOF */
