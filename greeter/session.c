/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * Session handling
 */ 

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <syslog.h>
#include <sys/stat.h>

#include "gdmlogin.h"
#include "proto.h"
#include "gdm.h"

static const gchar RCSid[]="$Id$";


/* Configuration vars */
extern gchar *GdmSessionDir;

/* External vars */
extern gchar *curuser;

/* Local vars */
static gboolean save    = FALSE; /* User wants his session selection saved */
static gchar *defsess   = NULL;	 /* The sysadmin specified default session */
static gchar *lastsess  = NULL;	 /* Placeholder for user's last session */
static gchar *cursess   = NULL;	 /* The currently highlighted session */
static GSList *sessions = NULL;	 /* Sessions installed on this machine */


/**
 * gdm_session_list_lookup:
 * @l: List containing data
 * @data: String to find in list
 *
 * Looks up a string in the list
 */

static gboolean 
gdm_session_list_lookup (GSList *l, gchar *data)
{
    GSList *list = l;

    if (!list || !data)
	return FALSE;

    while (list) {
	if (!strcasecmp (list->data, data))
	    return TRUE;
	
	list = list->next;
    }

    return FALSE;
}


/**
 * gdm_session_save:
 *
 * Return whether to save selection or not
 */

gboolean 
gdm_session_save (void)
{
    return save;
}


/**
 * gdm_session_lookup:
 * @savedsess: User's saved session
 *
 * Select and appropriate session for this session
 */

gchar *
gdm_session_lookup (gchar *savedsess)
{
    gchar *retval;

    if (!curuser)
	gdm_login_abort ("gdm_session_lookup: curuser==NULL. Mail <mkp@mkp.net> with " \
			 "information on your PAM and user database setup");

    /* Don't save session unless told otherwise */
    save = FALSE;

    /* Disable selection during processing */
    gdm_gui_sessmenu_deactivate();

    /* Previously saved session not found in ~user/.gnome/gdm */
    if (!savedsess || !*savedsess) {

	/* If "Last" is chosen run Default, else run user's current selection */
	if (!strcasecmp (cursess, lastsess))
	    retval = defsess;
	else
	    retval = cursess;

	save = TRUE;
	return retval;
    }

    /* If "Last" session is selected */
    if (!strcasecmp (cursess, lastsess)) { 
	retval = savedsess;

	/* Check if user's saved session exists on this box */
	if (!gdm_session_list_lookup (sessions, retval)) {
	    gchar *text;

	    retval = defsess;
	    text = g_strdup_printf (_("Your preferred session type %s is not installed on this machine.\n" \
				      "Do you wish to make %s the default for future sessions?"),
				    savedsess, defsess);	    
	    save = gdm_gui_query (text);
	    g_free (text);
	}
    }

    /* One of the other available session types is selected */
    else { 
	retval = cursess;

	/* User's saved session is not the chosen one */
	if (strcasecmp (savedsess, retval)) {
	    gchar *text;

	    text = g_strdup_printf (_("You have chosen %s for this session, but your default setting is " \
				      "%s.\nDo you wish to make %s the default for future sessions?"),
				    cursess, savedsess, cursess);
	    save = gdm_gui_query (text);
	    g_free (text);
	}
    }

    return retval;
}


/**
 * gdm_session_handler:
 * @widget: Widget that got selected
 *
 * Mark selected session
 */

void 
gdm_session_handler (GtkWidget *widget) 
{
    gchar *s;

    gtk_label_get (GTK_LABEL (GTK_BIN (widget)->child), &cursess);
    s = g_strdup_printf (_("%s session selected"), cursess);
    gdm_gui_message_set (s);
    g_free (s);
}


/**
 * gdm_session_init:
 * @menu: Menu to stuff sessions in
 *
 * Discover desktop environments installed on this system and add them to menu
 */

void 
gdm_session_init (GtkWidget *menu)
{
    GSList *sessgrp = NULL;
    GtkWidget *item;
    DIR *sessdir;
    struct dirent *dent;
    struct stat statbuf;
    gint linklen;

    /* Create menu entry for ``Last'' */
    lastsess = _("Last");
    cursess = lastsess;
    item = gtk_radio_menu_item_new_with_label (NULL, lastsess);
    sessgrp = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (item));
    gtk_menu_append (GTK_MENU (menu), item);
    gtk_signal_connect (GTK_OBJECT (item), "activate", gdm_session_handler, NULL);
    gtk_widget_show (GTK_WIDGET (item));

    /* Separator */
    item = gtk_menu_item_new();
    gtk_menu_append (GTK_MENU (menu), item);
    gtk_widget_show (GTK_WIDGET (item));

    /* Check that session dir is readable */
    if (access (GdmSessionDir, R_OK|X_OK))
	gdm_login_abort (_("gdm_session_init: Session script directory not found!"));

    /* Read directory entries in session dir */
    sessdir = opendir (GdmSessionDir);
    dent = readdir (sessdir);

    while (dent != NULL) {
	gchar *s;

	/* Ignore backups and rpmsave files */
	if ((strstr (dent->d_name, "~")) || (strstr (dent->d_name, ".rpmsave"))) {
	    dent = readdir (sessdir);
	    continue;
	}

	s = g_strconcat (GdmSessionDir, "/", dent->d_name, NULL);
	lstat (s, &statbuf);

	/* If default session link exists, find out what it points to */
	if (S_ISLNK (statbuf.st_mode) && !strcasecmp (dent->d_name, "default")) {
	    gchar t[_POSIX_PATH_MAX];
	    
	    linklen = readlink (s, t, _POSIX_PATH_MAX);
	    t[linklen] = 0;
	    defsess = g_strdup (t);
	}

	/* If session script is readable/executable add it to the list */
	if (S_ISREG (statbuf.st_mode)) {

	    if ((statbuf.st_mode & (S_IRUSR|S_IXUSR)) == (S_IRUSR|S_IXUSR) &&
		(statbuf.st_mode & (S_IRGRP|S_IXGRP)) == (S_IRGRP|S_IXGRP) &&
		(statbuf.st_mode & (S_IROTH|S_IXOTH)) == (S_IROTH|S_IXOTH)) 
	    {
		item = gtk_radio_menu_item_new_with_label (sessgrp, dent->d_name);
		sessgrp = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (item));
		sessions = g_slist_append (sessions, dent->d_name);
		gtk_menu_append (GTK_MENU (menu), item);
		gtk_signal_connect (GTK_OBJECT (item), "activate",
				    GTK_SIGNAL_FUNC (gdm_session_handler), NULL);
		gtk_widget_show (GTK_WIDGET (item));
	    }
	    else 
		syslog (LOG_ERR, "Wrong permissions on %s/%s. Should be readable/executable for all.", 
			GdmSessionDir, dent->d_name);

	}

	dent = readdir (sessdir);
	g_free (s);
    }

    if (!g_slist_length (sessgrp)) 
	gdm_login_abort (_("No session scripts found. Aborting!"));

    if (!defsess) {
	gtk_label_get (GTK_LABEL (GTK_BIN (g_slist_nth_data (sessgrp, 0))->child), &defsess);
	syslog (LOG_WARNING, _("No default session link found. Using %s.\n"), defsess);
    }
}


/* EOF */
