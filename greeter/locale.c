/* GDM - The Gnome Display Manager
 * Copyright (C) 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * Helper functions for maintaining the language/locale data structures.
 *
 * Meta: 
 *       The languages (GdmLang) are stored in a singly linked list.
 *
 *       Each GdmLang has a language (i.e. english) and one or more
 *       GdmLocales associated.  
 *
 *       A GdmLocale consists of a country (i.e. Canada), and a
 *       suitable locale string (en_CA.ISO8859-1).  
 */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gdmlogin.h"
#include "gdm.h"
#include "proto.h"

static const gchar RCSid[]="$Id$";


/* External vars */
extern gchar *GdmLocaleFile;
extern gint   GdmLangMenu;

/* Local vars */
GSList *languages;


/**
 * gdm_locale_lang_alloc:
 * @name: Name of the language to add
 *
 * Allocates a new language struct
 */

static GdmLang *
gdm_locale_lang_alloc (const gchar *name)
{
    GdmLang *lang;

    if (!name)
	return NULL;

    lang = g_new0 (GdmLang, 1);

    if (!lang)
	return NULL;

    lang->name = g_strdup (name);
    lang->locales = NULL;

    if (!lang->name)
	return NULL;

    return lang;
}


/**
 * gdm_locale_lang_sort_func:
 * @d1, @d2: pointers to GdmLang structs to compare
 *
 * Sort function for GdmLangs
 */

static gint 
gdm_locale_lang_sort_func (gpointer d1, gpointer d2)
{
    GdmLang *a = d1;
    GdmLang *b = d2;

    if (!d1 || !d2)
	return 0;

    return strcmp (a->name, b->name);
}


/**
 * gdm_locale_lang_lookup:
 * @name: Name of the language to lookup
 *
 * Searches for the language in question and returns a pointer to the
 * appropriate GdmLang struct.  
 */

GdmLang *
gdm_locale_lang_lookup (const gchar *name)
{
    GSList *l = languages;

    if (!name)
	return NULL;

    while (l && l->data) {
	GdmLang *lang = (GdmLang *) l->data;

	if (! strcasecmp (lang->name, name))
	    return (GdmLang *) lang;

	l = l->next;
    }

    return NULL;
}


/**
 * gdm_locale_lang_list:
 *
 * Return pointer to the language list
 */

GSList *
gdm_locale_lang_list (void)
{
    return languages;
}


/**
 * gdm_locale_alloc:
 * @country: Name of the country to add
 * @locale: Locale used in that country
 *
 * Allocates a new locale struct
 */

static GdmLocale *
gdm_locale_alloc (const gchar *country, const gchar *locale)
{
    GdmLocale *loc = NULL;

    if (!country || !locale)
	return NULL;

    loc = g_new0 (GdmLocale, 1);

    if (!loc)
	return NULL;

    loc->country = g_strdup (country);
    loc->localestr = g_strdup (locale);

    if (!loc->country || !loc->localestr)
	return NULL;

    return loc;
}


/**
 * gdm_locale_sort_func:
 * @d1, @d2: pointers to GdmLocale structs to compare
 *
 * Sort function for GdmLocales
 */

static gint 
gdm_locale_sort_func (gpointer d1, gpointer d2)
{
    GdmLocale *a = d1;
    GdmLocale *b = d2;

    if (!d1 || !d2)
	return 0;

    return strcmp (a->country, b->country);
}


/**
 * gdm_locale_append:
 * @name: Name of the language to append to
 * @country: Country to append
 * @locale: Locale used in that country
 * 
 * Appends a locale to the array.
 */

static GdmLocale *
gdm_locale_append (const gchar *name, const gchar *country, const gchar *locale)
{
    GdmLang *lang = NULL;
    GdmLocale *loc = NULL;

    if (!name || !country || !locale)
	return NULL;

    /* Does the language exist already? */
    lang = gdm_locale_lang_lookup (name);

    /* If it doesn't, create it */
    if (!lang) {
	lang = gdm_locale_lang_alloc (name);
	languages = g_slist_insert_sorted (languages, lang, 
					   (GCompareFunc) gdm_locale_lang_sort_func);
    }

    /* Couldn't create language */
    if (!lang || !languages)
	return NULL;

    /* Allocate locale */
    loc = gdm_locale_alloc (country, locale);

    if (!loc)
	return NULL;

    /* Append it to the appropriate language */
    lang->locales = g_slist_insert_sorted (lang->locales, loc,
					   (GCompareFunc) gdm_locale_sort_func);
    
    return loc;
}


/**
 * gdm_locale_init:
 *
 * Reads config file and initializes locale data structures
 */

void
gdm_locale_init (void)
{
    FILE *langlist;
    char curline[256];

    langlist = fopen (GdmLocaleFile, "r");

    if (!langlist) {
	return;
    }

    /* Parse locales.conf */
    while (fgets (curline, sizeof (curline), langlist)) {
	gchar **array = NULL;

	array = g_strsplit (curline, ":", 3);

	if (!array[0] || !array[1] || !array[2])
	    continue;

	g_strchomp (array[2]);

	if (! gdm_locale_append (array[0], array[1], array[2]))
	    gdm_login_abort ("gdm_locale_init: Could not append locale");
    }
    
    fclose (langlist);

    if (g_slist_length (languages) < 1)
	gdm_login_abort ("No suitable locales found in %s. " \
			 "Perhaps your locale configuration is using the old format?");
}


/* EOF */
