/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * User handling stuff for the face browser
 */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <syslog.h>

#include "gdmlogin.h"
#include "gdm.h"
#include "proto.h"
#include "../daemon/filecheck.h"

static const gchar RCSid[]="$Id$";


/* External vars */
extern gboolean GdmBrowser;
extern gchar *GdmGlobalFaceDir;
extern gchar *GdmDefaultFace;
extern gchar *GdmInclude;
extern gchar *GdmExclude;
extern gint GdmIconMaxWidth;
extern gint GdmIconMaxHeight;
extern GList *users;
extern gint maxwidth;

/* Local vars */
static GdkImlibImage *defface;
static GSList *exclude = NULL;
static GSList *include = NULL;
static gboolean incmode = FALSE;


/**
 * gdm_user_alloc:
 *
 * Allocate username/icon/etc
 */

static GdmLoginUser * 
gdm_user_alloc (gchar *logname, uid_t uid, gchar *homedir)
{
    GdmLoginUser *user;
    gint fileok;
    gchar *gnomedir = NULL;
    gchar *filename = NULL;
    GdkImlibImage *img = NULL;

    if (!logname || uid < 0 || !homedir)
	return NULL;

    /* Allocate new user struct */
    user = g_new0 (GdmLoginUser, 1);

    if (!user)
	return NULL;

    user->uid = uid;
    user->login = g_strdup (logname);
    user->homedir = g_strdup (homedir);
    gnomedir = g_strconcat (homedir, "/.gnome", NULL);

    /* Check for photo in user's homedir */
    fileok = gdm_file_check ("gdm_user_alloc", uid, gnomedir, "photo");
    
    if (fileok != -1) {	
	filename = g_strconcat (gnomedir, "/photo", NULL);
	img = gdk_imlib_load_image (filename);
	
	close (fileok);
    }
    /* Try in global dir */
    else {
	filename = g_strconcat (GdmGlobalFaceDir, "/", logname, NULL);
	
	if (access (filename, R_OK) == 0)
	    img = gdk_imlib_load_image (filename);
    }
    
    g_free (filename);
    g_free (gnomedir);
    
    /* If an image was found, scale it */
    if (img) {
	gint w, h;
	
	w = img->rgb_width;
	h = img->rgb_height;
	
	if (w > h && w > GdmIconMaxWidth) {
	    h = h * ((gfloat) GdmIconMaxWidth / w);
	    w = GdmIconMaxWidth;
	} 
	else if (h > GdmIconMaxHeight) {
	    w = w * ((gfloat) GdmIconMaxHeight / h);
	    h = GdmIconMaxHeight;
	}
	
	maxwidth = MAX (maxwidth, w);
	user->picture = gdk_imlib_clone_scaled_image (img, w, h);
	gdk_imlib_destroy_image (img);
    }
    else
	user->picture = defface;

    return user;
}


/**
 * gdm_user_sort_func:
 *
 * Sort function for GdmLoginUsers
 */

static gint 
gdm_user_sort_func (gpointer d1, gpointer d2)
{
    GdmLoginUser *a = d1;
    GdmLoginUser *b = d2;

    if (!d1 || !d2)
	return 0;

    return strcmp (a->login, b->login);
}


/**
 * gdm_user_check_list:
 *
 * Check whether the user given in pwent is on the list l
 */

static gint
gdm_user_check_list (GSList *l, struct passwd *pwent)
{
    GSList *list = l;

     while (list && list->data) {
	 if (! strcasecmp (pwent->pw_name, (gchar *) list->data))
	     return TRUE;

	 list = list->next;
     }

     return FALSE;
}


/**
 * gdm_user_check_shell:
 *
 * Check whether the user given in pwent has a valid shell
 */

static gint
gdm_user_check_shell (struct passwd *pwent)
{
    gint found = 0;
    gchar *csh;

    setusershell ();

    while ((csh = getusershell ()) != NULL)
	if (! strcmp (csh, pwent->pw_shell))
	    found = 1;

    endusershell ();

    return found;
}


/**
 * gdm_user_check_passwd:
 *
 * Check whether the user given in pwent has a valid encrypted password
 */

static gint
gdm_user_check_passwd (struct passwd *pwent)
{
    const gchar * const lockout_passes[] = { "*", "!!", NULL };
    gint i;

    for (i=0 ; lockout_passes[i] ; i++) 
	if (! strcmp (lockout_passes[i], pwent->pw_passwd))
	    return FALSE;

    return TRUE;
}


/**
 * gdm_user_init:
 *
 * Check whether the user given in pwent has a valid encrypted password
 */

void 
gdm_user_init (void)
{
    GdmLoginUser *user;
    struct passwd *pwent;

    if (!GdmBrowser || !GdmDefaultFace || !GdmGlobalFaceDir) {
	GdmBrowser = 0;
	return;
    }

    /* Check that default face can be read */
    if (access (GdmDefaultFace, R_OK)) {
	syslog (LOG_WARNING, _("Can't open DefaultImage: %s. Suspending face browser!"), 
		GdmDefaultFace);
	GdmBrowser = 0;
	return;
    }

    /* Read it in */
    defface = gdk_imlib_load_image (GdmDefaultFace);

    if (!defface) {
	syslog (LOG_WARNING, _("Can't read DefaultImage: %s. Suspending face browser!"), 
		GdmDefaultFace);
	GdmBrowser = 0;
	return;
    }

    /* Include has precedence over Exclude */
    if (GdmInclude) {
        gchar *s = strtok (GdmInclude, ",");
        include = g_slist_append (include, g_strdup (s));

        while ((s = strtok (NULL, ","))) 
	    include = g_slist_append (include, g_strdup (s));

	incmode = TRUE;
    }
    else if (GdmExclude) {
        gchar *s = strtok (GdmExclude, ",");
        exclude = g_slist_append (exclude, g_strdup (s));

        while ((s = strtok (NULL, ","))) 
	    exclude = g_slist_append (exclude, g_strdup (s));

	incmode = FALSE;
    }

    pwent = getpwent();
	
    /* Iterate over logins */
    while ( (pwent = getpwent()) != NULL) {
	
	/* Valid shell and password */
	if (pwent->pw_shell && gdm_user_check_shell (pwent) &&
	    pwent->pw_passwd && gdm_user_check_passwd (pwent))	{

	    /* Include/Exclude handling */
	    if ((incmode && !gdm_user_check_list (include, pwent)) ||
		(!incmode && gdm_user_check_list (exclude, pwent)))
		continue;

	    user = gdm_user_alloc (pwent->pw_name, pwent->pw_uid, pwent->pw_dir);

	    if ((user) && (! g_list_find_custom (users, user, (GCompareFunc) gdm_user_sort_func)))
		users = g_list_insert_sorted(users, user, (GCompareFunc) gdm_user_sort_func);
	}
    }    
}


/* EOF */
