/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GDM_H__
#define __GDM_H__

#include <glib.h>
#include <fcntl.h>
#include <X11/Xlib.h>
#include <X11/Xmd.h>
#include <X11/Xauth.h>

#ifndef O_NOFOLLOW
  #define O_NOFOLLOW 0
#endif

#define STX             0x2	/* Start of txt */

#define TYPE_LOCAL        1	/* Local X server */
#define TYPE_XDMCP        2	/* Remote display */

#define SERVER_SUCCESS    0	/* X server default */
#define SERVER_FAILURE    1	/* X server default */
#define SERVER_NOTFOUND 127	/* X server default */
#define SERVER_DEAD     128	/* Server stopped */
#define SERVER_STARTED  129	/* Server started but not ready for connections yet */
#define SERVER_RUNNING  130	/* Server running and ready for connections */
#define SERVER_TIMEOUT  131	/* Server timed out accepting connections */
#define SERVER_RETEX    132	/* Number of retries exceeded */
#define SERVER_REMANAGE 133	/* Display requests to be remanaged */
#define SERVER_NOP      134	/* Don't do anything */
#define SERVER_ABORT    135	/* Server failed badly. Suspending display. */
#define SERVER_RESET    136	/* Attempting server reset */
#define SERVER_MANAGED  137	/* Server being managed */
#define SERVER_REBOOT   253	/* Reboot machine */
#define SERVER_HALT     254	/* Halt machine */

#define DISPLAY_SUCCESS   0	/* All systems are go */
#define DISPLAY_REMANAGE  2	/* Restart display */
#define DISPLAY_ABORT     4	/* Houston, we have a problem */
#define DISPLAY_REBOOT    8	/* Rebewt */
#define DISPLAY_HALT     16	/* Halt */
#define DISPLAY_DEAD     32	/* Display not configured/started yet */

#define XDMCP_DEAD        0
#define XDMCP_PENDING     1
#define XDMCP_MANAGED     2


/* Opcodes for the highly sophisticated protocol used for
 * daemon<->greeter interprocess communication */
#define GDM_MSGERR 'D'
#define GDM_NOECHO 'U'
#define GDM_PROMPT 'N'
#define GDM_SESS   'G'
#define GDM_LANG   '&'
#define GDM_SSESS  'C'
#define GDM_SLANG  'R'
#define GDM_RESET  'A'
#define GDM_QUIT   'P'
#define GDM_STOP   '!'

/* The dreaded miscellaneous category */
#define MAX_ARGS 32
#define FIELD_SIZE 128
#define PIPE_SIZE 1024

/* Configuration constants */
#define GDM_KEY_CHOOSER "daemon/Chooser=gdmchooser"
#define GDM_KEY_GREETER "daemon/Greeter=gdmlogin"
#define GDM_KEY_GROUP "daemon/Group=gdm"
#define GDM_KEY_HALT "daemon/HaltCommand=shutdown -h now"
#define GDM_KEY_INITDIR "daemon/DisplayInitDir="
#define GDM_KEY_KILLIC "daemon/KillInitClients=1"
#define GDM_KEY_LOGDIR "daemon/LogDir="
#define GDM_KEY_PATH "daemon/DefaultPath=/bin:/usr/bin:/usr/bin/X11:/usr/local/bin"
#define GDM_KEY_PIDFILE "daemon/PidFile=/var/run/gdm.pid"
#define GDM_KEY_POSTSESS "daemon/PostSessionScriptDir="
#define GDM_KEY_PRESESS "daemon/PreSessionScriptDir="
#define GDM_KEY_REBOOT "daemon/RebootCommand=shutdown -r now"
#define GDM_KEY_ROOTPATH "daemon/RootPath=/sbin:/usr/sbin:/bin:/usr/bin:/usr/bin/X11:/usr/local/bin"
#define GDM_KEY_SERVAUTH "daemon/ServAuthDir=/var/gdm"
#define GDM_KEY_SESSDIR "daemon/SessionDir="
#define GDM_KEY_UAUTHDIR "daemon/UserAuthDir="
#define GDM_KEY_UAUTHFB "daemon/UserAuthFBDir=/tmp/"
#define GDM_KEY_USER "daemon/User=gdm"

#define GDM_KEY_ALLOWROOT "security/AllowRoot=1"
#define GDM_KEY_MAXFILE "security/UserMaxFile=65536"
#define GDM_KEY_RELAXPERM "security/RelaxPermissions=0"
#define GDM_KEY_RETRYDELAY "security/RetryDelay=3"
#define GDM_KEY_VERBAUTH "security/VerboseAuth=1"

#define GDM_KEY_XDMCP "xdmcp/Enable=1"
#define GDM_KEY_MAXPEND "xdmcp/MaxPending=4"
#define GDM_KEY_MAXSESS "xdmcp/MaxSessions=16"
#define GDM_KEY_MAXWAIT "xdmcp/MaxWait=30"
#define GDM_KEY_DISPERHOST "xdmcp/DisplaysPerHost=1"
#define GDM_KEY_UDPPORT "xdmcp/Port=177"

#define GDM_KEY_GTKRC "gui/GtkRC="
#define GDM_KEY_ICONWIDTH "gui/MaxIconWidth=128"
#define GDM_KEY_ICONHEIGHT "gui/MaxIconHeight=128"

#define GDM_KEY_BROWSER "greeter/Browser=0"
#define GDM_KEY_GRAB "greeter/GrabFocus=1"
#define GDM_KEY_EXCLUDE "greeter/Exclude=bin,daemon,adm,lp,sync,shutdown,halt,mail,news,uucp,operator,nobody"
#define GDM_KEY_INCLUDE "greeter/Include="
#define GDM_KEY_FACE "greeter/DefaultFace=nobody.png"
#define GDM_KEY_FACEDIR "greeter/GlobalFaceDir="
#define GDM_KEY_FONT "greeter/Font=-adobe-helvetica-bold-r-normal-*-*-180-*-*-*-*-*-*"
#define GDM_KEY_ICON "greeter/Icon=gdm.xpm"
#define GDM_KEY_LANGMENU "greeter/LanguageMenu=1"
#define GDM_KEY_LOCALE "greeter/DefaultLocale=english"
#define GDM_KEY_LOCFILE "greeter/LocaleConf="
#define GDM_KEY_LOGO "greeter/Logo="
#define GDM_KEY_QUIVER "greeter/Quiver=1"
#define GDM_KEY_SESSMENU "greeter/SessionMenu=1"
#define GDM_KEY_SYSMENU "greeter/SystemMenu=0"
#define GDM_KEY_WELCOME "greeter/Welcome=Welcome to %h"
#define GDM_KEY_XPOSITION "greeter/XPosition="
#define GDM_KEY_YPOSITION "greeter/YPosition="

#define GDM_KEY_SCAN "chooser/ScanTime=3"
#define GDM_KEY_HOST "chooser/DefaultHostImg=nohost.png"
#define GDM_KEY_HOSTDIR "chooser/HostImageDir="

#define GDM_KEY_DEBUG "debug/Enable=0"

#define GDM_KEY_SERVERS "servers"


typedef struct _GdmDisplay GdmDisplay;

struct _GdmDisplay {
    CARD32 sessionid;
    Display *dsp;
    gchar *authfile;
    GSList *auths; 
    gchar *userauth;
    gchar *command;
    gchar *cookie;
    gchar *bcookie;
    gchar *name;
    gchar *hostname;
    guint8 dispstat;
    guint16 dispnum;
    guint8 servstat;
    guint8 servtries;
    guint8 type;
    pid_t greetpid;
    pid_t servpid;
    pid_t sesspid;
    pid_t slavepid;
    time_t acctime;
};

#endif /* __GDM_H__ */

/* EOF */
