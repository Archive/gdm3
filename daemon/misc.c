/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>
#include <syslog.h>

#include "gdm.h"
#include "misc.h"

static const gchar RCSid[]="$Id$";


/* Configuration option variables */
extern gchar *GdmPidFile;
extern gint GdmDebug;


/**
 * gdm_fail:
 * @format: printf style format string
 * @...: Optional arguments
 *
 * Logs fatal error condition and aborts master daemon.  Also sleeps
 * for 30 seconds to avoid looping if gdm is started by init.  
 */

void 
gdm_fail (const gchar *format, ...)
{
    va_list args;
    gchar *s;

    va_start (args, format);
    s = g_strdup_vprintf (format, args);
    va_end (args);

    /* Log to both syslog and stderr */
    syslog (LOG_ERR, s);
    fprintf (stderr, "%s\n", s);
    fflush (stderr);

    g_free (s);

    unlink (GdmPidFile);
    closelog ();

    /* Slow down respawning if we're started from init */
    if (getppid() == 1)
	sleep (30);

    exit (EXIT_FAILURE);
}


/**
 * gdm_info:
 * @format: printf style format string
 * @...: Optional arguments
 *
 * Log non-fatal information to syslog
 */

void 
gdm_info (const gchar *format, ...)
{
    va_list args;
    gchar *s;

    va_start (args, format);
    s = g_strdup_vprintf (format, args);
    va_end (args);
    
    syslog (LOG_INFO, "%s", s);
    
    g_free (s);
}


/**
 * gdm_error:
 * @format: printf style format string
 * @...: Optional arguments
 *
 * Log non-fatal error condition to syslog
 */

void 
gdm_error (const gchar *format, ...)
{
    va_list args;
    gchar *s;

    va_start (args, format);
    s = g_strdup_vprintf (format, args);
    va_end (args);
    
    syslog (LOG_ERR, "%s", s);
    
    g_free (s);
}


/**
 * gdm_debug:
 * @format: printf style format string
 * @...: Optional arguments
 *
 * Log debug information to syslog if debugging is enabled.
 */

void 
gdm_debug (const gchar *format, ...)
{
    va_list args;
    gchar *s;

    if (! GdmDebug) 
	return;

    va_start (args, format);
    s = g_strdup_vprintf (format, args);
    va_end (args);
    
    syslog (LOG_ERR, "%s", s);	/* FIXME: LOG_DEBUG */
    
    g_free (s);
}


/**
 * gdm_setenv:
 * @var: Variable to set/unset
 * @value: Value to assign to the variable, NULL for unsetting
 */

gint 
gdm_setenv (const gchar *var, const gchar *value) 
{
    static gchar *envstr = NULL;  /* Temporary environment string */
    gint result;		  /* Return value from the putenv() call */

    /* `var' is a prerequisite */
    if (!var && !*var)
	return -1;

    /* If `value' is specified then set the variable accordingly. Otherwise clear it. */
    if (value && *value)
	envstr = g_strconcat (var, "=", value, NULL);
    else
	envstr = g_strconcat (var, "=", NULL);

    /* If string space allocation failed then abort */
    if (!envstr)
	return -1;

    /* Stuff the resulting string into the environment */
    result = putenv (envstr);

    return result;
}


/* EOF */
