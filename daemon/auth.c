/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Code for cookie handling. This really needs to be modularized to
 * support other XAuth types and possibly DECnet... */

#include <config.h>
#include <gnome.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netdb.h> 
#include <netinet/in.h>
#include <X11/Xauth.h>

#include "gdm.h"
#include "cookie.h"
#include "misc.h"
#include "filecheck.h"
#include "auth.h"

static const gchar RCSid[]="$Id$";


/* Local prototypes */
static void gdm_auth_purge (GdmDisplay *d, FILE *af);
static FILE * gdm_auth_user_file (GdmDisplay *d, uid_t user, gchar *homedir);


/* Configuration option variables */
extern gchar *GdmServAuthDir;
extern gchar *GdmUserAuthDir;
extern gchar *GdmUserAuthFB;
extern gint  GdmUserMaxFile;
extern gint  GdmRelaxPerms;


/**
 * gdm_auth_secure_display:
 * @d: Pointer to a GdmDisplay struct
 * 
 * Create authentication cookies for local and remote displays.
 *
 * Returns TRUE on success and FALSE on error.
 */

gboolean
gdm_auth_secure_display (GdmDisplay *d)
{
    gint afd;
    FILE *af;
    struct hostent *hentry;
    struct in_addr *ia;
    gchar *addr;
    Xauth *xa;
    guint i;

    if (!d)
	return FALSE;

    gdm_debug ("gdm_auth_secure_display: Setting up access for %s", d->name);

    if (! d->authfile)
	d->authfile = g_strconcat (GdmServAuthDir, "/", d->name, ".Xauth", NULL);

    unlink (d->authfile);
    afd = open (d->authfile, O_CREAT|O_WRONLY|O_APPEND|O_EXCL|O_NOFOLLOW, 0640);

    if (afd == -1)
	return FALSE;

    af = fdopen (afd, "w");

    if (!af)
	return FALSE;

    /* If this is a local display the struct hasn't changed and we
     * have to eat up old authentication cookies before baking new
     * ones... */
    if (d->type == TYPE_LOCAL && d->auths) {
	GSList *alist = d->auths;

	while (alist && alist->data) {
	    XauDisposeAuth ((Xauth *) alist->data);
	    alist = alist->next;
	}

	g_slist_free (d->auths);
	d->auths = NULL;

	if (d->cookie)
	    g_free (d->cookie);

	if (d->bcookie)
	    g_free (d->bcookie);
    }

    /* Create new random cookie */
    gdm_cookie_generate (d);

    /* Find FQDN or IP of display host */
    hentry = gethostbyname (d->hostname);

    if (! hentry) {
	gdm_error ("gdm_auth_secure_display: Error getting hentry for %s", d->hostname);
	return FALSE;
    }

    /* Local access */
    if (d->type == TYPE_LOCAL) {
	gdm_debug ("gdm_auth_secure_display: Setting up socket access");

	xa = g_new0 (Xauth, 1);
	
	if (!xa)
	    return FALSE;

	xa->family = FamilyLocal;
	xa->address = strdup (d->hostname);
	xa->address_length = strlen (d->hostname);
	xa->number = g_strdup_printf ("%d", d->dispnum);
	xa->number_length = 1;
	xa->name = strdup ("MIT-MAGIC-COOKIE-1");
	xa->name_length = 18;
	xa->data = strdup (d->bcookie);
	xa->data_length = strlen (d->bcookie);
	XauWriteAuth (af, xa);
	d->auths = g_slist_append (d->auths, xa);
    }

    gdm_debug ("gdm_auth_secure_display: Setting up network access");
    
    /* Network access: Write out an authentication entry for each of
     * this host's official addresses */
    for (i = 0 ; i < hentry->h_length ; i++) {
	xa = g_new0 (Xauth, 1);

	if (! xa)
	    return FALSE;

	xa->family = FamilyInternet;

	addr = g_new0 (gchar, 4);

	if (!addr)
	    return FALSE;

	ia = (struct in_addr *) hentry->h_addr_list[i];

	if (!ia)
	    break;

	memcpy (addr, &ia->s_addr, 4);
	xa->address = addr; 
	xa->address_length = 4;
	xa->number = g_strdup_printf ("%d", d->dispnum);
	xa->number_length = 1;
	xa->name = strdup ("MIT-MAGIC-COOKIE-1");
	xa->name_length = 18;
	xa->data = strdup (d->bcookie);
	xa->data_length = strlen (d->bcookie);

	XauWriteAuth (af, xa);

	d->auths = g_slist_append (d->auths, xa);
    }

    fclose (af);
    close (afd);

    gdm_setenv ("XAUTHORITY", d->authfile);

    gdm_debug ("gdm_auth_secure_display: Setting up access for %s - %d entries", 
	       d->name, g_slist_length (d->auths));

    return TRUE;
}


/**
 * gdm_auth_user_add:
 * @d: Pointer to a GdmDisplay struct
 * @user: Userid of the user whose cookie file to add entries to
 * @homedir: The user's home directory
 * 
 * Remove all cookies referring to this display from user's cookie
 * file and append the ones specified in the display's authlist.
 *
 * Returns TRUE on success and FALSE on error.  
 */

gboolean
gdm_auth_user_add (GdmDisplay *d, uid_t user, gchar *homedir)
{
    FILE *af;
    GSList *auths = NULL;

    if (!d)
	return FALSE;

    /* Only the user should be able to read the cookie */
    umask (077);

    /* Find a suitable cookie file */
    af = gdm_auth_user_file (d, user, homedir);

    if (!af)
	return FALSE;

    gdm_debug ("gdm_auth_user_add: Using %s for cookies for %d", d->userauth, user);

    /* Nuke any existing cookies for this display */
    gdm_auth_purge (d, af);

    /* Append the authlist for this display to the cookie file */
    auths = d->auths;

    while (auths) {
	XauWriteAuth (af, auths->data);
	auths = auths->next;
    }

    fclose (af);
    XauUnlockAuth (d->userauth);
    gdm_setenv ("XAUTHORITY", d->userauth);

    gdm_debug ("gdm_auth_user_add: Done");

    return TRUE;
}


/**
 * gdm_auth_user_remove:
 * @d: Pointer to a GdmDisplay struct
 * @user: Userid of the user whose cookie file to remove entries from
 * 
 * Remove all cookies referring to this display from user's cookie
 * file.
 */

void 
gdm_auth_user_remove (GdmDisplay *d, uid_t user)
{
    gint afd;
    FILE *af;
    gchar *authfile, *authdir;

    if (!d || !d->userauth)
	return;

    gdm_debug ("gdm_auth_user_remove: Removing cookie from %s", d->userauth);

    authfile = g_basename (d->userauth);
    authdir = g_dirname (d->userauth);

    /* Now, the cookie file could be owned by a malicious user who
     * decided to concatenate something like his entire MP3 collection
     * to it. So we better play it safe... */

    afd = gdm_file_check ("gdm_auth_user_remove", user, authdir, authfile);

    if (afd == -1)
	goto done;

    /* Lock user's cookie jar and open it for writing */
    if (XauLockAuth (d->userauth, 3, 3, 0) != LOCK_SUCCESS)
	goto done;

    af = fdopen (afd, "a+");

    if (!af)
	goto done;

    /* Purge entries for this display from the cookie jar */
    gdm_auth_purge (d, af);

    /* Close the file and unlock it */
    fclose (af);
    close (afd);

 done:
    XauUnlockAuth (d->userauth);

    g_free (authdir);
    g_free (d->userauth);
    d->userauth = NULL;

    return;
}


/**
 * gdm_auth_purge:
 * @d: Pointer to a GdmDisplay struct
 * @af: File handle to a cookie file
 * 
 * Remove all cookies referring to this display a cookie file.
 */

static void
gdm_auth_purge (GdmDisplay *d, FILE *af)
{
    Xauth *xa;
    GSList *keep = NULL;

    if (!d || !af)
	return;

    gdm_debug ("gdm_auth_purge: %s", d->name);

    fseek (af, 0L, SEEK_SET);

    /* Read the user's entire Xauth file into memory to avoid
     * temporary file issues. Then remove any instance of this display
     * in the cookie jar... */

    while ( (xa = XauReadAuth (af)) ) {
	gboolean match = FALSE;
	GSList *alist = d->auths;

	while (alist) {
	    Xauth *da = alist->data;

	    if (! memcmp (da->address, xa->address, xa->address_length) &&
		! memcmp (da->number, xa->number, xa->number_length))
		match = TRUE;

	    alist = alist->next;
	}

	if (match)
	    XauDisposeAuth (xa);
	else
	    keep = g_slist_append (keep, xa);
    }

    /* Rewind the file */
    af = freopen (d->userauth, "w", af);

    if (!af) 
	goto done;

    /* Write out remaining entries */
    while (keep) {
	XauWriteAuth (af, keep->data);
	XauDisposeAuth (keep->data);
	keep = keep->next;
    }

 done:
    if (keep)
	g_slist_free (keep);
}


/**
 * gdm_auth_user_file:
 * @d: Pointer to a GdmDisplay struct
 * @user: User's uid
 * @homedir: User's home directory
 * 
 * Find a suitable cookie file for the user. Returns an open FILE*.
 */

static FILE *
gdm_auth_user_file (GdmDisplay *d, uid_t user, gchar *homedir)
{
    gint afd = -1;
    gboolean tmpfile = TRUE;

    if (!d || !*homedir) 
	return NULL;

    /* If GdmUserAuthDir isn't specified ~/.Xauthority is used.
     * In the event that the paranoia check fails, GdmUserAuthFB is used instead.
     *
     * If GdmUserAuthDir was specified use that for cookie storage.
     */

    if (!*GdmUserAuthDir) {

	afd = gdm_file_check ("gdm_auth_user_file", user, homedir, ".Xauthority");

	if (afd != -1) {	/* Check ok */
	    d->userauth = g_strconcat (homedir, "/.Xauthority", NULL);
	    tmpfile = FALSE;
	}
	else			/* Check failed */
	    d->userauth = g_strconcat (GdmUserAuthFB, "/.gdmXXXXXX", NULL);
    }
    else			/* Sysadming specified cookie dir */
	d->userauth = g_strconcat (GdmUserAuthDir, "/.gdmXXXXXX", NULL);

    if (tmpfile)
	afd = mkstemp (d->userauth);
    
    if (afd == -1 || XauLockAuth (d->userauth, 3, 3, 0) != LOCK_SUCCESS) {

	gdm_error (_("gdm_auth_user_file: Could not open cookie file %s"), d->userauth);
	g_free (d->userauth);
	d->userauth = NULL;

	return NULL;
    }

    return fdopen (afd, "w+");
}


/* EOF */
