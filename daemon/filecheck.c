/* GDM - The Gnome Display Manager
 * Copyright (C) 1998, 1999, 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "gdm.h"
#include "filecheck.h"

static const gchar RCSid[]="$Id$";

extern gint GdmRelaxPerms;
extern gint GdmUserMaxFile;

/**
 * gdm_file_check:
 * @caller: String to be prepended to syslog error messages.
 * @user: User id for the user owning the file/dir.
 * @dir: Directory to be examined.
 * @file: File to be examined.
 *
 * Examines a file to determine whether it is safe for the daemon to write to it.
 * Returns open fd on success and -1 on failure.
 */

gint
gdm_file_check (gchar *caller, uid_t user, gchar *dir, gchar *file)
{
    gint fd;
    struct stat statbuf;
    gchar *fullpath;

    /* Stat directory */
    if (stat (dir, &statbuf) == -1) {
	syslog (LOG_WARNING, _("%s: Directory %s does not exist."), caller, dir);
	return -1;
    }

    /* Check if dir is owned by the user ... */
    if (statbuf.st_uid != user) {
	syslog (LOG_WARNING, _("%s: %s is not owned by uid %d."), caller, dir, user);
	return -1;
    }
    
    /* ... if group has write permission ... */
    if (GdmRelaxPerms < 1 && (statbuf.st_mode & S_IWGRP) == S_IWGRP) {
	syslog (LOG_WARNING, _("%s: %s is writable by group."), caller, dir);
	return -1;
    }

    /* ... and if others have write permission. */
    if (GdmRelaxPerms < 2 && (statbuf.st_mode & S_IWOTH) == S_IWOTH) {
	syslog (LOG_WARNING, _("%s: %s is writable by other."), caller, dir);
	return -1;
    }

    fullpath = g_strconcat (dir, "/", file, NULL);
    fd = open (fullpath, O_CREAT|O_RDWR|O_NOFOLLOW, 0600);

    if (fd == -1)
	return -1;

    if (fstat (fd, &statbuf) == -1)
	return -1;

    /* Check that it is a regular file ... */
    if (! S_ISREG (statbuf.st_mode)) {
	syslog (LOG_WARNING, _("%s: %s is not a regular file."), caller, fullpath);
	g_free (fullpath);
	return -1;
    }

    /* ... owned by the user ... */
    if (statbuf.st_uid != user) {
	syslog (LOG_WARNING, _("%s: %s is not owned by uid %d."), caller, fullpath, user);
	g_free (fullpath);
	return -1;
    }

    /* ... unwritable by group ... */
    if (GdmRelaxPerms < 1 && (statbuf.st_mode & S_IWGRP) == S_IWGRP) {
	syslog (LOG_WARNING, _("%s: %s is writable by group."), caller, fullpath);
	g_free (fullpath);
	return -1;
    }

    /* ... unwritable by others ... */
    if (GdmRelaxPerms < 2 && (statbuf.st_mode & S_IWOTH) == S_IWOTH) {
	syslog (LOG_WARNING, _("%s: %s is writable by group/other."), caller, fullpath);
	g_free (fullpath);
	return -1;
    }

    /* ... and smaller than sysadmin specified limit. */
    if (GdmUserMaxFile && statbuf.st_size > GdmUserMaxFile) {
	syslog (LOG_WARNING, _("%s: %s is bigger than sysadmin specified maximum file size."), 
		caller, fullpath);
	g_free (fullpath);
	return -1;
    }

    g_free (fullpath);

    /* Yeap, this file is ok */
    return fd;
}

/* EOF */
