/* GDM - The Gnome Display Manager
 * Copyright (C) 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* This file contains functions for controlling local X servers */

#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <strings.h>
#include <signal.h>
#include <errno.h>
#include <X11/Xlib.h>

#include "gdm.h"
#include "server.h"
#include "misc.h"
#include "xdmcp.h"
#include "display.h"
#include "auth.h"

static const gchar RCSid[]="$Id$";


/* Local prototypes */
void gdm_server_spawn (GdmDisplay *d);
gboolean gdm_server_dispatch (gpointer data);
void gdm_server_stop (GdmDisplay *d);
void gdm_server_reset (GdmDisplay *d);
void gdm_server_term_handler (gint);
void gdm_server_hup_handler (gint);
void gdm_server_usr1_handler (gint);
void gdm_server_child_handler (gint);

/* Configuration options and external vars */
extern gchar *argdelim;
extern gchar *GdmDisplayInit;
extern gchar *GdmLogDir;
extern gchar *GdmServAuthDir;
extern gint GdmXdmcp;
extern sigset_t sysmask;
extern sigset_t gdmmask;
extern GMainLoop *main_loop;    

/* Global vars */
GdmDisplay *d;
GMainLoop *dispatch;


/**
 * gdm_server_start:
 * @disp: Pointer to a GdmDisplay structure
 *
 * Starts a local X server. Handles retries and fatal errors properly.
 */

gboolean
gdm_server_start (GdmDisplay *disp)
{
    struct sigaction term, usr1, chld;
    sigset_t mask;
    gchar *logfile = NULL;
    int logfd;

    if (!disp || disp->type != TYPE_LOCAL)
	return FALSE;

    d = disp;

    /* Fork server slave process */
    switch (d->slavepid = fork()) {

    case -1:
        gdm_error (_("gdm_server_start: Failed forking slave process for %d"), d->name);
        return FALSE;

    case 0:
        break;

    default:
        gdm_debug ("gdm_server_start: Forked server slave: %d", d->slavepid);
	return TRUE;
    }

    /* Stop main daemon event handler */
    g_main_quit (main_loop);
    gdm_debug ("gdm_server_start: %s", d->name);

    if (GdmXdmcp)
	gdm_xdmcp_close();
    
    /* Log all output from spawned programs to a file */
    logfile = g_strconcat (GdmLogDir, "/", d->name, ".log", NULL);
    unlink (logfile);
    logfd = open (logfile, O_CREAT|O_TRUNC|O_APPEND|O_WRONLY|O_EXCL|O_NOFOLLOW, 0600);
    g_free (logfile);

    if (logfd != -1) {
	dup2 (logfd, STDOUT_FILENO);
	dup2 (logfd, STDERR_FILENO);
	close (logfd);
    }
    else
	gdm_error (_("gdm_server_start: Could not open logfile for display %s!"), d->name);
    
    /* Catch TERM/INT from master daemon */
    term.sa_handler = gdm_server_term_handler;
    term.sa_flags = SA_RESTART|SA_RESETHAND;
    sigemptyset (&term.sa_mask);
    
    if (sigaction (SIGTERM, &term, NULL) < 0) {
	gdm_error (_("gdm_server_start: Error setting up TERM signal handler"));
	exit (SERVER_FAILURE);
    }

    if (sigaction (SIGINT, &term, NULL) < 0) {
	gdm_error (_("gdm_server_start: Error setting up INT signal handler"));
	exit (SERVER_FAILURE);
    }

    if (sigaction (SIGHUP, &term, NULL) < 0) {
	gdm_error (_("gdm_server_start: Error setting up HUP signal handler"));
	exit (SERVER_FAILURE);
    }

    /* Catch USR1 from X server */
    usr1.sa_handler = gdm_server_usr1_handler;
    usr1.sa_flags = SA_RESTART|SA_RESETHAND;
    sigemptyset (&usr1.sa_mask);
    
    if (sigaction (SIGUSR1, &usr1, NULL) < 0) {
	gdm_error (_("gdm_server_start: Error setting up USR1 signal handler"));
	exit (SERVER_FAILURE);
    }

    /* Catch CHLD from X server */
    chld.sa_handler = gdm_server_child_handler;
    chld.sa_flags = SA_RESTART|SA_RESETHAND|SA_NOCLDSTOP;
    sigemptyset (&chld.sa_mask);

    if (sigaction (SIGCHLD, &chld, NULL) < 0) {
	gdm_error (_("gdm_server_start: Error setting up CHLD signal handler"));
	exit (SERVER_FAILURE);
    }

    /* Set signal mask */
    sigemptyset (&mask);
    sigaddset (&mask, SIGTERM);
    sigaddset (&mask, SIGINT);
    sigaddset (&mask, SIGHUP);
    sigaddset (&mask, SIGUSR1);
    sigaddset (&mask, SIGCHLD);
    sigprocmask (SIG_UNBLOCK, &mask, NULL);

    d->servpid = 0;
    d->servtries = 0;

    /* Start main dispatch */
    dispatch = g_main_new (FALSE);
    g_timeout_add (5000, (GSourceFunc) gdm_server_dispatch, NULL);
    g_main_run (dispatch);

    /* Prevent returning to master daemon */
    while (1)
	pause();

    /* We'll never return from here */
    gdm_debug ("gdm_server_start: Point of no return for %d", d->name);
    return TRUE;
}


/**
 * gdm_server_dispatch:
 * @data: Not used
 *
 * Server control master event handler
 */

gboolean
gdm_server_dispatch (gpointer data) 
{
    switch (d->servstat) {

    case SERVER_DEAD:		/* Server isn't running */
	gdm_debug ("gdm_server_dispatch: SERVER_DEAD");
	gdm_server_spawn (d);
	break;

    case SERVER_STARTED:	/* Server started but not ready yet. We just wait. */
	gdm_debug ("gdm_server_dispatch: SERVER_STARTED");
	break;
	
    case SERVER_RUNNING:	/* Server alive and kicking */
	gdm_debug ("gdm_server_dispatch: SERVER_RUNNING");
	d->servstat = SERVER_MANAGED;
	gdm_display_manage (d);
	break;

    case SERVER_TIMEOUT:	/* Server timed out accepting connections */
	gdm_debug ("gdm_server_dispatch: SERVER_TIMEOUT");
	gdm_server_spawn (d);
	break;

    case SERVER_RETEX:		/* Number of retries exceeded */
	gdm_debug ("gdm_server_dispatch: SERVER_RETEX");
	d->servstat = SERVER_NOP;
	sleep (5*60);
	d->servstat = SERVER_DEAD;
	break;

    case SERVER_RESET:		/* Restart login session */
	gdm_debug ("gdm_server_dispatch: SERVER_RESET");
	gdm_display_unmanage (d);
	gdm_server_spawn (d);
	break;

    case SERVER_NOP:		/* Do nothing for a while */
	gdm_debug ("gdm_server_dispatch: SERVER_NOP");
	break;

    case SERVER_MANAGED:	/* Slave is running */
	gdm_debug ("gdm_server_dispatch: SERVER_MANAGED");
	break;

    case SERVER_ABORT:		/* Terminate services of this display */
	gdm_debug ("gdm_server_dispatch: SERVER_ABORT");
	gdm_display_unmanage (d);
	gdm_server_stop (d);

	exit (SERVER_ABORT);

    case SERVER_REBOOT:		/* Reboot machine */
	gdm_debug ("gdm_server_dispatch: SERVER_REBOOT");
	gdm_display_unmanage (d);
	gdm_server_stop (d);

	exit (SERVER_REBOOT);

    case SERVER_HALT:		/* Halt machine */
	gdm_debug ("gdm_server_dispatch: SERVER_HALT");
	gdm_display_unmanage (d);
	gdm_server_stop (d);

	exit (SERVER_HALT);

    default:
	gdm_debug ("gdm_server_dispatch: Unknown server status %d", d->servstat);
	break;
    }

    return TRUE;
}


/**
 * gdm_server_spawn:
 * @disp: Pointer to a GdmDisplay structure
 *
 * Resets existing X server or forks a new X server process
 */

void 
gdm_server_spawn (GdmDisplay *d)
{
    struct sigaction usr1;
    gchar *srvcmd = NULL;
    gchar **argv = NULL;

    if (!d)
	return;

    /* Increase spawn counter */
    d->servtries++;

    /* Are we looping? */
    if (d->servtries > 5) {
	d->servstat = SERVER_RETEX; /* Then sleep for a while */
	return;
    }

    gdm_debug ("gdm_server_spawn: %s", d->name);

    /* Secure display */
    if (! gdm_auth_secure_display (d)) {
	d->servstat = SERVER_ABORT;
	return;
    }

    gdm_setenv ("DISPLAY", d->name);
    gdm_setenv ("XAUTHORITY", d->authfile);

    /* Check if a server is already running for this display */
    if (d->servpid && kill (d->servpid, 0) == 0) {

	/* Server running, let's try to reset it... */
	if ((d->servstat != SERVER_RESET) && (d->servtries < 2)) {
	    gdm_debug ("gdm_server_spawn: Attempting reset for %s (%d)", d->name, d->servpid);

	    d->servstat = SERVER_RESET;
	    kill (d->servpid, SIGHUP);

	    return;
	}
	/* Our previous attempt to reset the server failed. Let's kill
	 * Kenny the hard way... 
	 */
	else 
	    gdm_server_stop (d);
    }

    /* Fork into two processes. Parent remains the gdm process. Child
     * becomes the X server. 
     */

    d->servstat = SERVER_STARTED;
    
    switch (d->servpid = fork()) {

    case 0:
	/* The X server expects USR1 to be SIG_IGN */
	usr1.sa_handler = SIG_IGN;
	usr1.sa_flags = SA_RESTART;
	sigemptyset (&usr1.sa_mask);
	
	if (sigaction (SIGUSR1, &usr1, NULL) < 0) {
	    gdm_error (_("gdm_server_spawn: Error setting USR1 to SIG_IGN"));
	    exit (SERVER_FAILURE);
	}
	
	srvcmd = g_strconcat (d->command, " -auth ", GdmServAuthDir, \
			      "/", d->name, ".Xauth ", 
			      d->name, NULL);
	
	gdm_debug ("gdm_server_spawn: '%s'", srvcmd);
	
	argv = g_strsplit (srvcmd, argdelim, MAX_ARGS);
	g_free (srvcmd);

	/* Ideally we would not need this */
	setgid (0);
	setuid (0);
	setpgid (0, 0);
	execv (argv[0], argv);
	
	gdm_error (_("gdm_server_spawn: X server not found: %s"), d->command);
	exit (SERVER_FAILURE);
	
    case -1:
	gdm_error (_("gdm_server_spawn: Can't fork X server process!"));
	d->servpid = 0;
	d->servstat = SERVER_ABORT;
	return;
	
    default:
	d->servstat = SERVER_STARTED;
	break;
    }
}


/**
 * gdm_server_stop:
 * @disp: Pointer to a GdmDisplay structure
 *
 * Stops a local X server
 */

void
gdm_server_stop (GdmDisplay *d)
{
    sigset_t mask, omask;

    if (!d || d->type != TYPE_LOCAL)
	return;

    gdm_debug ("gdm_server_stop: %s", d->name);

    /* Ignore annoying signals */
    sigemptyset (&mask);
    sigaddset (&mask, SIGCHLD);
    sigprocmask (SIG_BLOCK, &mask, &omask);

    /* Whack */
    kill (d->servpid, SIGTERM);
    waitpid (d->servpid, 0, 0);

    sigprocmask (SIG_SETMASK, &omask, NULL);

    d->servpid = 0;
    unlink (d->authfile);
}


/**
 * gdm_server_term_handler:
 * @sig: Signal value
 *
 * Nuke the running X server
 */

void
gdm_server_term_handler (gint sig)
{
    d->servstat = SERVER_ABORT;	/* Stop managing this display */
}


/**
 * gdm_server_usr1_handler:
 * @sig: Signal value
 *
 * Received when the server is ready to accept connections
 */

void
gdm_server_usr1_handler (gint sig)
{
    d->servstat = SERVER_RUNNING; /* Server ready to accept connections */
    d->servtries = 0;
}


/**
 * gdm_server_child_handler:
 * @sig: Signal value
 *
 * Received when server died during startup
 */

void 
gdm_server_child_handler (gint signal)
{
    gint exitstatus = 0, status = 0;
    pid_t pid = waitpid (-1, &exitstatus, WNOHANG);

    if (pid < 1)
	return;

    if (WIFEXITED (exitstatus))
	status = WEXITSTATUS (exitstatus);
	
    /* Server died. We don't care. Slave will complain at some point. */
    if (pid == d->servpid) {
	d->servstat = SERVER_NOP;
	return;
    }
    
    /* Slave process barfed */
    d->slavepid = 0;
    d->dispstat = DISPLAY_DEAD;

    /* Autopsy */
    switch (status) {

    case DISPLAY_ABORT:		/* Bury this display for good */
	d->servstat = SERVER_ABORT;
	return;
	
    case DISPLAY_REBOOT:	/* Reboot machine */
	d->servstat = SERVER_REBOOT;
	return;
	
    case DISPLAY_HALT:		/* Halt machine */
	d->servstat = SERVER_HALT;
	return;

    case DISPLAY_REMANAGE:	/* Remanage display */
    default:
	d->servstat = SERVER_RESET;
	break;
    }
}


/**
 * gdm_server_alloc:
 * @id: Local display number
 * @command: Command line for starting the X server
 *
 * Allocate display structure for a local X server
 */

GdmDisplay * 
gdm_server_alloc (gint id, gchar *command)
{
    gchar *dname = g_new0 (gchar, 1024);
    gchar *hostname = g_new0 (gchar, 1024);
    GdmDisplay *d = g_new0 (GdmDisplay, 1);
    
    if (gethostname (hostname, 1023) == -1)
	return NULL;

    sprintf (dname, ":%d", id);  
    d->authfile = NULL;
    d->auths = NULL;
    d->userauth = NULL;
    d->command = g_strdup (command);
    d->cookie = NULL;
    d->dispstat = DISPLAY_DEAD;
    d->greetpid = 0;
    d->name = g_strdup (dname);
    d->hostname = g_strdup (hostname);
    d->dispnum = id;
    d->servpid = 0;
    d->servstat = SERVER_DEAD;
    d->sessionid = 0;
    d->sesspid = 0;
    d->slavepid = 0;
    d->type = TYPE_LOCAL;
    d->sessionid = 0;
    d->acctime = 0;
    d->dsp = NULL;
    
    g_free (dname);
    g_free (hostname);

    return d;
}

/* EOF */
