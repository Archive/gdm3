/* GDM - The Gnome Display Manager
 * Copyright (C) 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* gdmgrab is a simple program which opens a connection to the $DISPLAY.
 * The gdm daemon calls gdmgrab to prevent managed displays from
 * resetting due to lack of active connections.  
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <X11/Xlib.h>

static const char RCSid[]="$Id$";


int 
main (void)
{
    char *envptr;
    Display *dsp = NULL;

    if (! (envptr = getenv ("DISPLAY")))
	exit (EXIT_FAILURE);

    if (! (dsp = XOpenDisplay (envptr)))
        exit (EXIT_FAILURE);

    while (1)
	pause();

    exit (EXIT_SUCCESS);
}


/* EOF */
