/* GDM - The Gnome Display Manager
 * Copyright (C) 2000 Martin K. Petersen <mkp@mkp.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* xkillall sends kill signals to all windows on a given display.
 * Based upon an xdm hack by Keith Packard. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <X11/Xlib.h>

static const char RCSid[]="$Id$";


int 
main (int argc, char *argv[])
{
    int retries = 0;
    Display *dsp = NULL;
    int screen = 0;

    /* Display name must be given as argument */
    if (argc < 2)
	exit (EXIT_FAILURE);

    /* Give it 5 shots */
    while (retries < 5) {
	dsp = XOpenDisplay (argv[1]);
	
	if (dsp) 
	    goto done;

	sleep (retries*2);
	retries++;
    }

    /* No go. Bail out */
    if (dsp == NULL) 
        exit (EXIT_FAILURE);

 done:    

    /* Recursively kill all windows on display */
    for (screen = 0 ; screen < ScreenCount (dsp) ; screen++) {
	Window root, parent, *children;
        int child = 0, nchildren = 0;
        
        while (XQueryTree (dsp, RootWindow (dsp, screen), &root, &parent,
                           &children, &nchildren) && nchildren>0) {
            
            for (child = 0 ; child < nchildren ; child++) 
                XKillClient (dsp, children[child]);
            
            XFree (children);
        }
    }
    
    XSync (dsp, 0);

    exit (EXIT_SUCCESS);
}


/* EOF */
